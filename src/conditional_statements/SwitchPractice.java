package conditional_statements;

import java.sql.SQLOutput;
import java.util.Scanner;

public class SwitchPractice {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Please enter a number from 1 to 6");

        int num = input.nextInt();

        if (num == 1){

            System.out.println("Jan");
        } else if (num == 2) {
            System.out.println("Feb");
        } else if (num == 3) {
            System.out.println("Mar");
        } else if (num == 4) {
            System.out.println("Apr");
        }else if (num == 5) {
            System.out.println("May");
        }else if (num == 6) {
            System.out.println("June");
        }else{
            System.out.println("This number is not between 1 - 6");
        }



        switch (num){
            case 1: {
                System.out.println("Jan");
                break;
            }
            case 2: {
                System.out.println("Feb");
                break;
            }
            case 3:{
                System.out.println("Mar");
                break;
            }
            case 4:{
                System.out.println("Apr");
                break;
            }
            case 5:{
                System.out.println("May");
                break;
            }
            case 6:{
                System.out.println("Jun");
                break;
            }
            default:
                System.out.println("This number is not between 1 - 6");
                break;
        }

        System.out.println("Please enter either a, b, or c");

        String letter = input.next();
        switch (letter){
            case "a":{
                System.out.println("You entered the letter a");
                break;
            }
            case "b":{
                System.out.println("You entered the letter b");
                break;
            }
            case "c":{
                System.out.println("You entered the letter c");
                break;
            }
            default:{
                System.out.println("Error this is not a letter from a to c");
                break;
            }
        }
        }

    }


