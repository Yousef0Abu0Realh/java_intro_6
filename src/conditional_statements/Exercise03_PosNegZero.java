package conditional_statements;

import java.util.Scanner;

public class Exercise03_PosNegZero {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Hey user, can give me a number");

        int num = input.nextInt();

        if(num < 0){
            System.out.println("Negative");

        } else if (num > 0) {
            System.out.println("Positive");
        }
        else{
            System.out.println("Zero");
        }
    }
}
