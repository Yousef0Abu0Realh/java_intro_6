package conditional_statements;

import java.util.Random;
import java.util.Scanner;

public class Practice {
    public static void main(String[] args) {

        System.out.println("\n Task 01 \n");

        Random r = new Random();

        int num1 = Math.round(r.nextInt(50));

        System.out.println(num1);

        System.out.println(((num1 >= 10 && num1 <= 25))?(true):(false));


        System.out.println("\n Task 02 \n");

        int num2 = Math.round(r.nextInt(99) + 1);

        if(num2 >= 1 && num2 <=50) {
            if (num2 >= 1 && num2 <= 25) {
                System.out.println(num2 + " is in the 1st half");
                System.out.println(num2 + " is in the 1st quarter");
            } else {
                System.out.println(num2 + " is in the 1st half");
                System.out.println(num2 + " is in the 2nd quarter");
            }
        }else{
            if( num2 >= 51 && num2 <= 75){
                System.out.println(num2 + " is in the 2nd half");
                System.out.println(num2 + " is in the 3rd quarter");
            }else{
                System.out.println(num2 + " is in the 2nd half");
                System.out.println(num2 + " is in the 4th quarter");
            }
        }

        System.out.println("\n Task 03 \n");

      char letter = 'a';

      if ((letter >= 65 && letter <= 90)||(letter >= 97 && letter <= 122))System.out.println("Character is a letter");
      else if(letter >= 48 && letter <= 57) System.out.println("Character is a digit");


    }
}
