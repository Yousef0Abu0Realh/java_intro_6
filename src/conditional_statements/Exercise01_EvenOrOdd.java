package conditional_statements;

import java.util.Scanner;

public class Exercise01_EvenOrOdd {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Hey user, give me a number");

        int num = input.nextInt();

        if( num % 2 == 0){
            System.out.println("The number is even");
        }
        else{
            System.out.println("The number is odd");

        }
    }
}
