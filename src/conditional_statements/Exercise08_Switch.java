package conditional_statements;


import java.util.Scanner;

public class Exercise08_Switch {
    public static void main(String[] args) {

    Scanner input = new Scanner(System.in);

    System.out.println("Hey user can you give me a day of the week in lower case");

    String r = input.next();

    switch(r){
        case "sunday":
            System.out.println("7th day of the week");
            break;
        case "monday":
            System.out.println("1th day of the week");
            break;
        case "tuesday":
            System.out.println("2th day of the week");
            break;
        case "wednesday":
            System.out.println("3th day of the week");
            break;
        case "thursday":
            System.out.println("4th day of the week");
            break;
        case "friday" :
            System.out.println("5th day of the week");
            break;
        case "saturday":
            System.out.println("6th day of the week");
            break;
        default:{
            System.out.println("Error this is not a day in the week");
        }
            break;

    }

}
}
