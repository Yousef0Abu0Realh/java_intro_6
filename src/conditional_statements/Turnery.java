package conditional_statements;

import java.util.Scanner;

public class Turnery {
    public static void main(String[] args) {
         int num1 = 5;
         int num2 = 10;

         int differenece;
        /* if (num1 > num2){
             differenece = num1 - num2;
         }else{
             differenece = num2 - num1;
         }
        System.out.println(differenece);
        */

        differenece = num1 > num2 ? num1 -num2 : num2 - num1;
        System.out.println(differenece);

        Scanner input = new Scanner(System.in);

        System.out.println("Please enter a number");
        int num = input.nextInt();

        /*

        if(num % 2 != 1){
            System.out.println("even");
        }else System.out.println("odd");

         */

        System.out.println((num % 2 != 1)?("Even"):("Odd"));
    }
}
