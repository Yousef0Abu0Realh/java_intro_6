package conditional_statements;

import java.util.Scanner;

public class Exercise02_MathScore {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Hey david, what did you score on the math test.");

        int score = input.nextInt();

        if(score >= 60){
            System.out.println("awsome! You have passed the math class!");
        }
        else{
            System.out.println("Sorry! you failed");
        }
    }
}
