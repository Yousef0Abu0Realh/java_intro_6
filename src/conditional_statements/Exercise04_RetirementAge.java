package conditional_statements;

import java.util.Scanner;

public class Exercise04_RetirementAge {
    public static void main (String[] args){

        Scanner input = new Scanner(System.in);

        System.out.println("Hey user, Can you tell me your age.");

        int age = input.nextInt();

        if (age >= 55){
            System.out.println("It's time to get retired !");
        }
        else{
          int ageLeft = 55 - age;

          if (ageLeft == 1){
              System.out.println("You have 1 year before retirement");
          }
          else{
              System.out.println("You have " + ageLeft + " years before retirement");
          }
        }

        System.out.println("End of program");

    }
}
