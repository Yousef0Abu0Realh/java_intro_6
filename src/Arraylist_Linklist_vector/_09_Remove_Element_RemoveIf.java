package Arraylist_Linklist_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class _09_Remove_Element_RemoveIf {
    public static void main(String[] args) {

        ArrayList<String> colors = new ArrayList<>(Arrays.asList("Red", "Purple", "Blue", "Yellow"));


        System.out.println(colors);

        colors.removeIf(element -> element.toUpperCase().contains("R"));

        System.out.println(colors);
    }
}
