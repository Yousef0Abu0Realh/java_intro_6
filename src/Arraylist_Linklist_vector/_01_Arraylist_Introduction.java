package Arraylist_Linklist_vector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class _01_Arraylist_Introduction {
    public static void main(String[] args) {


        //1. How to create an Array and Arraylist
        System.out.println("\n ---- Task 01 ---- \n");
        String[] array = new String[3];
        ArrayList<String> list = new ArrayList<>(); // capacity = 10 by default


        System.out.println("\n ---- Task 02 ---- \n");
        //2. how to get the size of an Array  and ArrayList
        System.out.println("The size of the Array = " + array.length);//3
        System.out.println("The size of the list = " + list.size());//0

        System.out.println("\n ---- Task 03 ---- \n");
        //3.how to print an Array vs Arraylist
        System.out.println("The array = " + Arrays.toString(array));//[null,null,null]
        System.out.println("The list = " + list);//[]

        System.out.println("\n ---- Task 04 ---- \n");
        //3.how to add elements to an Array vs Arraylist

        array[1] = "Alex";
        array[2] = "Max";
        array[0] = "John";

        System.out.println("The array = " + Arrays.toString(array));// [John, Alex, Max]

        list.add("Joe");
        list.add("Jane");
        list.add("Mike");
        list.add("Adam");

        list.add(2,"Jazzy");//adds to index of 2
        list.add(5,"Yahya"); // if the
        System.out.println("The list = " + list);// [Joe, Jane, Jazzy, Mike, Adam] doesn't sort the array automatically

        System.out.println("\n ---- Task 05 ---- \n");
        //5.How to update an existing element in an array and arraylist

        array[1] = "Ali";
        System.out.println("The array = " + Arrays.toString(array)); // [John, Ali, Max]

        list.set(1,"Jasmine");
        System.out.println("The list = " + list);//[Joe, Jasmine, Jazzy, Mike, Adam]

        System.out.println("\n ---- Task 06 ---- \n");
        // how to print out single array elements
        System.out.println(array[2]);
        System.out.println(list.get(3));

        System.out.println("\n ---- Task 07 ---- \n");
        //6. How to loop an array vs array list

        System.out.println("\n ---- fori loop ---- \n");

        //array
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
        System.out.println("\n");
        //arraylist
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        System.out.println("\n ---- for each loop ---- \n");
        // array
        for(String element : array){
            System.out.println(element);
        }

        System.out.println("\n");
        // arraylist
        for (String element : list) {
            System.out.println(element);
        }

        System.out.println("\n lambda expression \n");

        list.forEach(System.out::println); // lambda expression


    }
}
