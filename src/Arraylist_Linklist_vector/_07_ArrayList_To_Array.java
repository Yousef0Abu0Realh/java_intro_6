package Arraylist_Linklist_vector;

import java.util.ArrayList;

public class _07_ArrayList_To_Array {
    public static void main(String[] args) {

        System.out.println(uniques(new int []{3, 5, 7, 3, 5}));
        System.out.println(uniques(new int []{10, 10, 10, 10}));
        System.out.println(uniques(new int []{}));
        System.out.println(uniques(new int []{13, 20, 20, 13}));


    }

    public static ArrayList<Integer>  uniques(int[] arr){

        ArrayList<Integer> numbs2 = new ArrayList<>();

        for (int i : arr) {
            if(!numbs2.contains(i)) numbs2.add(i);
        }

        return numbs2;
    }
}
