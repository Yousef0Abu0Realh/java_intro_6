package Arraylist_Linklist_vector;

import java.util.Arrays;
import java.util.LinkedList;

public class _06_LinkedList_Introduction {
    public static void main(String[] args) {
        LinkedList<String> cities = new LinkedList<>(Arrays.asList("Berlin","Rome","Kyiv","Ankara","Madrid","Chicago"));

        System.out.println(cities.size());
        System.out.println(cities.contains("Miami"));
        System.out.println(cities.indexOf("Evanston"));

        System.out.println(cities.getFirst());
        System.out.println(cities.getLast());

        System.out.println(cities.removeFirst());
        System.out.println(cities.removeLast());

        System.out.println(cities);

        System.out.println(cities.pop());

        System.out.println(cities);

        cities.push("Barcelona");

        System.out.println(cities);
    }
}
