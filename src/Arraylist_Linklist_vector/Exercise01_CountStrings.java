package Arraylist_Linklist_vector;

import string_Methods.Exercise01;

import java.util.ArrayList;
import java.util.Arrays;

public class Exercise01_CountStrings {
    public static void main(String[] args) {

        ArrayList<String> words = new ArrayList<>(Arrays.asList("Hello","Hi","School","Computer"));
        ArrayList<String> words2 = new ArrayList<>(Arrays.asList("abc","xyz"));
        ArrayList<String> empty = new ArrayList<>();
        ArrayList<String> words3 = new ArrayList<>(Arrays.asList("Object","Laptop"));

        System.out.println(countO(words));
        System.out.println(more3(words));
        System.out.println(more3(words2));
        System.out.println(more3(words3));
        System.out.println(more3(empty));

    }

    public static int countO (ArrayList<String> list){

        int counter = 0;

        for (String s : list) {

            if(s.toUpperCase().contains("O")) counter++;
        }

        return (counter);

    }

    public static int more3 (ArrayList<String> list){

        int count3 = 0;

        for (String element : list) {
            if (element.length() >= 3)count3++;

        }
        return count3;

    }
}
