package Arraylist_Linklist_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class _03_Group_Methods {
    public static void main(String[] args) {

        ArrayList<String> group1Members = new ArrayList<>();
        group1Members.add("Belal");
        group1Members.add("Assem");
        group1Members.add("Gurkan");
        group1Members.add("Dima");

        ArrayList<String> group2Members = new ArrayList<>();
        group2Members.add("Adam");
        group2Members.add("Melek");
        group2Members.add("Cihan");

        ArrayList<String> group3Members = new ArrayList<>();
        group3Members.add("Yousef");
        group3Members.add("Sandina");


        System.out.println("Group 1 members = " + group1Members);
        System.out.println("Group 2 members = " + group2Members);
        System.out.println("Group 3 members = " + group3Members);

        System.out.println("\n--- addAll() ---\n");
        ArrayList<String> allMembers = new ArrayList<>();
        allMembers.addAll(group1Members);
        allMembers.addAll(group2Members);
        allMembers.addAll(0,group3Members);

        System.out.println("All members = " + allMembers);

        System.out.println("\n--- removeAll()/RemoveIf() ---\n");

        ArrayList<String> elementsToBeRemoved = new ArrayList<>();
        elementsToBeRemoved.add("Adam");
        elementsToBeRemoved.add("Assem");
        elementsToBeRemoved.add("Yousef");

        allMembers.removeAll(elementsToBeRemoved);
        //allMembers.removeIf(element -> element.equals("Adam") || element.equals("Assem") || element.equals("Yousef"));

        System.out.println(allMembers);

        System.out.println("\n ---- containsAll() ----\n");

        ArrayList<String> check1 = new ArrayList<>(Arrays.asList("Cihan","Dima","Sandina"));
        ArrayList<String> check2 = new ArrayList<>(Arrays.asList("Jazzy","Gurkan","Belal","Melek"));

        System.out.println(allMembers.containsAll(check1));
        System.out.println(allMembers.containsAll(check2));

        System.out.println("\n ---- retainsAll() ----\n");
        ArrayList<String> keepList = new ArrayList<>(Arrays.asList("Cihan", "Gurkan"));

        allMembers.retainAll(keepList);

        System.out.println(allMembers);

        //allMembers.removeIf(element -> !element.equals("Gurkan") && !element.equals("Cihan"));



    }
}
