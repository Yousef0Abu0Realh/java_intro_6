package Arraylist_Linklist_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class _08_Arraylist_to_Array {
    public static void main(String[] args) {
        ArrayList<String> languages = new ArrayList<>(Arrays.asList());


        languages.add("Java");
        languages.add("C#");
        languages.add("JS");
        languages.add("Ruby");

        System.out.println(languages);

        Object[] myArray = languages.toArray();

        System.out.println(Arrays.toString(myArray));

        System.out.println(myArray.length);
    }
}
