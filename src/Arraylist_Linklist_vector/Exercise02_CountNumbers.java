package Arraylist_Linklist_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class Exercise02_CountNumbers {
    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(22,33,25));

        System.out.println(countEven(numbers));
       Exercise02_CountNumbers.more15(numbers);



    }

    public static int countEven(ArrayList<Integer> evens){

        int countEven = 0;

        for (Integer even : evens) {

            if(even % 2 == 0)countEven++;

        }
        return countEven;

    }

    public static int no3s(ArrayList<Integer>list){
        int count3 = 0;
        for (Integer integer : list) {

       if(integer.toString().contains("3"))count3++;

        }

        return count3;
    }


    public static int countEven2(ArrayList<Integer> list){
        return (int) list.stream().filter(element -> element % 2 == 0).count();
    }

    public static int moreThan15(ArrayList<Integer> list){

        return (int) list.stream().filter(element -> element > 15).count();
    }

    public static void more15(ArrayList<Integer> list){

        int count = 0;

        for (Integer integer : list) {

            if (integer > 15) count++;

        }
        System.out.println(count);
    }
}
