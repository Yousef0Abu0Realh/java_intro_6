package Arraylist_Linklist_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class _02_Additional_Methods {
    public static void main(String[] args) {

        System.out.println("\n --- Task 01 --- \n");

        ArrayList<Integer> numbers = new ArrayList<>();

        numbers.add(10);
        numbers.add(15);
        numbers.add(20);
        numbers.add(10);
        numbers.add(20);
        numbers.add(30);

        System.out.println(numbers);
        System.out.println(numbers.size());


        System.out.println("\n--- contains method --- \n");

        System.out.println(numbers.contains(5));
        System.out.println(numbers.contains(10));
        System.out.println(numbers.contains(20));
        System.out.println(numbers.indexOf(45) != -1);

        System.out.println("\n --- indexOf() and lastIndexOf() method --- \n");

        System.out.println(numbers.indexOf(10));
        System.out.println(numbers.indexOf(20));
        System.out.println(numbers.lastIndexOf(10));
        System.out.println(numbers.lastIndexOf(20));

        System.out.println(numbers.indexOf(21));
        System.out.println(numbers.lastIndexOf(21));


        System.out.println("\n --- remove method --- \n");

        numbers.remove((Integer)15);
        numbers.remove((Integer)30);
        numbers.remove((Integer)10);

        System.out.println(numbers);


        numbers.removeIf(element -> element == 20); // lambda

        System.out.println(numbers);

        numbers.add(11);
        numbers.add(12);
        numbers.add(13);

        System.out.println("\n --- clear() & removeAll() method --- \n");
        numbers.clear();

        //numbers.removeAll(numbers);

        System.out.println(numbers);


    }
}
