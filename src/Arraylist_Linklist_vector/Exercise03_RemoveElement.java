package Arraylist_Linklist_vector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class Exercise03_RemoveElement {
    public static void main(String[] args) {
        ArrayList<String> words = new ArrayList<>(Arrays.asList("Hello","World","Gym"));

        System.out.println(removeVowels(words));

        System.out.println(removeVowles3(words));


    }

    public static ArrayList<String> removeVowels(ArrayList<String> words){


        words.removeIf(element -> element.toLowerCase().contains("a") || element.toLowerCase().contains("e") || element.toLowerCase().contains("i") ||element.toLowerCase().contains("o") || element.toLowerCase().contains("u"));

        return words;
    }

    public static ArrayList<String> removeVowles3 (ArrayList<String> list){

        Iterator<String> iterator = list.iterator();

        while(iterator.hasNext()){
            String currentElement = iterator.next();
            if(currentElement.toLowerCase().contains("a") || currentElement.toLowerCase().contains("e") || currentElement.toLowerCase().contains("i") || currentElement.toLowerCase().contains("o") || currentElement.toLowerCase().contains("u")) iterator.remove();
        }

        return list;
    }
}
