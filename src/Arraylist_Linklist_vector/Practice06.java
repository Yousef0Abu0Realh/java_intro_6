package Arraylist_Linklist_vector;

import java.util.*;

public class

Practice06 {
    public static void main(String[] args) {
        System.out.println("==== task 01 ====");

        int[] numbers = {3,2,5,7,0};
        System.out.println(Arrays.toString(doubleNumber(new int[]{3,2,5,7,0})));

        ArrayList<Integer> numbers2 = new ArrayList<>(Arrays.asList(2,3,7,1,1,7,1));

        System.out.println(secondMax(numbers2));
        System.out.println(secondMin(numbers2));
        ArrayList<String> words = new ArrayList<>(Arrays.asList("Tech", "Global", "",null, "", "School"));

        System.out.println(removeEmpty(words));

        ArrayList<Integer> numbers3 = new ArrayList<>(Arrays.asList(200, 5, 100, 99, 101, 75));

        System.out.println(lessThan100(numbers3));

        String sentence = "Star Bright Star Light" ;

        System.out.println(uniqueWords(sentence));


    }
    // task01
    public static int[] doubleNumber(int[]numbs){

        for (int i = 0; i < numbs.length; i++) {
            numbs[i] *= 2;
        }

        return numbs;
    }

    // task02

    public static int secondMax(ArrayList<Integer> numbs){

        int max1 = Integer.MIN_VALUE;
        int secondMax = Integer.MIN_VALUE;

        Collections.sort(numbs);

        for (Integer numb : numbs) {

            max1 = Math.max(max1,numb);

        }

        for (Integer numb : numbs) {
            if(numb > secondMax && numb != max1)secondMax = numb;
        }

        return secondMax;
    }

    public static int secondMin(ArrayList<Integer> numbs) {

        int min1 = Integer.MAX_VALUE;
        int secondMin = Integer.MAX_VALUE;

        Collections.sort(numbs);

        for (Integer numb : numbs) {

            min1 = Math.min(min1, numb);

        }

        for (Integer numb : numbs) {
            if (numb < secondMin && numb != min1) secondMin = numb;
        }

        return secondMin;
    }


    public static int secondMin2(ArrayList<Integer> list){

        Collections.sort(list);
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) > list.get(0)) return list.get(i);

        }
        return 0;

    }

    public static ArrayList<String> removeEmpty(ArrayList<String> words){
        words.removeIf(Objects::isNull);
        words.removeIf(String::isEmpty);

      return words;

    }

   /*

   secondary way but doesn't remove null
   public static ArrayList<String> removeEmpty2(ArrayList<String> words){


        ArrayList<String> noEmpty = new ArrayList<>();

        for (String s : words) {
            if(!s.isEmpty())noEmpty.add(s);
        }

        return noEmpty;

    }

    */

    public static ArrayList<Integer> lessThan100(ArrayList<Integer> numbs){
        numbs.removeIf(element -> Math.abs(element) >= 100);
        return numbs;
    }

    public static ArrayList<String> uniqueWords(String words){

      ArrayList<String> strAsList = new ArrayList<>();

        for (String s : words.split(" ")) {

            if (!strAsList.contains(s)) strAsList.add(s);

        }

        return strAsList;

    }



}
