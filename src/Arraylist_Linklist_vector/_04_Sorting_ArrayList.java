package Arraylist_Linklist_vector;

import java.lang.reflect.Array;
import java.util.*;

public class _04_Sorting_ArrayList {
    public static void main(String[] args) {

        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(10, 15, 5, 2, -7 ));

        LinkedList<String> colors = new LinkedList<>(Arrays.asList("Red","Yellow","Brown"));

        Vector<Double> balance = new Vector<>(Arrays.asList(10.3, 5.7, 10.0, 15.6));


        System.out.println("\n --- Print collections before sorting --- \n");

        System.out.println(numbers);
        System.out.println(colors);
        System.out.println(balance);

        System.out.println("\n --- Print collections after sorting --- \n");

        Collections.sort(numbers);
        Collections.sort(colors);
        Collections.sort(balance);


        System.out.println(numbers);
        System.out.println(colors);
        System.out.println(balance);




    }
}
