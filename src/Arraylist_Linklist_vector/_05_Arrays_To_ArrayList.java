package Arraylist_Linklist_vector;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class _05_Arrays_To_ArrayList {
    public static void main(String[] args) {

        /*
      HOW TO CONVERT AN ARRAY TO AN ARRAYLIST
      1. Arrays.asList() method
       */

        System.out.println("\n----------Way-1 asList() method-------\n");
        ArrayList<String> cities = new ArrayList<>(Arrays.asList("Berlin", "Paris", "Rome"));

        System.out.println(cities); // [Berlin, Paris, Rome]


        System.out.println("\n----------Way-2-------\n");
        String[] countries = {"USA", "Germany", "Spain", "Italy"};

        ArrayList<String> list = new ArrayList<>();


       // for (String country : countries) {

            //list.add(country);

       // }

        System.out.println("\n----------Way-3-------\n");

        Collections.addAll(list,countries);

        System.out.println(list);
    }
}
