package Arraylist_Linklist_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class _10_Remove_Elements_Loop {
    public static void main(String[] args) {

        ArrayList<String> colors = new ArrayList<>(Arrays.asList("Red", "Purple", "Blue", "Yellow"));

        System.out.println(colors);

       /* for (String color : colors) {

            if(color.toUpperCase().contains("R"))colors.remove(color);

        }


        */

        ArrayList<String> newlist = new ArrayList<>();

        for (String color : colors) {

            if(!color.toUpperCase().contains("R"))newlist.add(color);

        }

        System.out.println(newlist);
    }
}
