package method;

import utilities.ScannerHelper;

public class CheckName {
    public static void main(String[] args) {


        ScannerHelper scannerHelper = new ScannerHelper();

        String name = ScannerHelper.getFirstName();

        System.out.println("\nThe name entered by the user = " + name);

        String fullName = ScannerHelper.getFullName();

        System.out.println("The full name entered is =  " + fullName);




    }

}
