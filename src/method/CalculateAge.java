package method;

import utilities.ScannerHelper;

public class CalculateAge {
    public static void main(String[] args) {

        int age = ScannerHelper.getAge(), num = ScannerHelper.giveNumber();

        System.out.println("age will be " + (age + num) + " after " + (num) + " years.");

    }
}
