package method;

import utilities.MathHelper;
import utilities.RandomGenerator;

public class Exercise01 {
    public static void main(String[] args) {
        int r1 = RandomGenerator.getRandomNumber(5,22);
        int r2 = RandomGenerator.getRandomNumber(3,22);
        int r3 = RandomGenerator.getRandomNumber(10,22);

        System.out.println(r1);
        System.out.println(r2);
        System.out.println(r3);

        System.out.println(MathHelper.maxOfThree(r1,r2,r3));
    }
}
