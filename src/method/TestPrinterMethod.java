package method;

import utilities.MathHelper;
import utilities.Printer;

import java.util.Scanner;

public class TestPrinterMethod {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        Printer.printGM();

        System.out.println("Hey user tell me your name");

        String name = input.nextLine();

        Printer.helloName(name);

        Printer.helloName("John");

        Printer myprinter = new Printer();
        myprinter.printTechGlobal();

        System.out.println(myprinter);

        MathHelper.sum(3,5);

        Printer.printGM();
    }
}
