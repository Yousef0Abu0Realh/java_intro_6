package utilities;

import java.util.Scanner;

public class ScannerHelper {

    static Scanner input = new Scanner(System.in);

    public static String getFirstName() {

        System.out.println("Please enter your first name");

        Scanner input = new Scanner(System.in);

        return input.nextLine();
    }

    public static String getLastName(){
        System.out.println("Please enter your last name");

        Scanner input = new Scanner(System.in);

        return input.nextLine();

    }

    public static String getFullName(){

        System.out.println("Hey user, can you give me your full name");

        String fullName = input.nextLine();

        input.nextLine();

        return fullName;


    }
    public static int getAge(){

        System.out.println("Hey user can you give me your age?");

        int age = input.nextInt();


        return age;
    }

    public static int giveNumber(){
        System.out.println("Hey user can you give me a number");

        int num = input.nextInt();

        return num;
    }

    public static String getTheSame(){
        System.out.println("Hey user, write something");

        String s1 = input.nextLine();

        System.out.println("Now enter Something else");

        String s2 = input.nextLine();

        String result = (s1.equals(s2) ? "these string are equal" : "These Strings are not equal");

        return result;
    }

    public static int favBookAndQuote(){
        System.out.println("Hey user can you tell me your favorite book");
        String favBook = input.nextLine();
        System.out.println("now tell me a quote from the book");
        String quote = input.nextLine();

        String fullBook = favBook.concat(quote);

        int length = fullBook.length();
        return length;

    }

    public static String getString(){

        String s1 = input.nextLine();

        return s1;
    }

    public static String getAddress(){
        System.out.println("Hey user, tell me your address");

        String address = input.nextLine();

        return address;
    }

    public static int getnumber(){
        System.out.println("Hey user give me a number");

        int num = input.nextInt();

        return num;
    }

    }
