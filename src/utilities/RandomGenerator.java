package utilities;

import java.util.Random;

public class RandomGenerator {
    public static int getRandomNumber(int start,int end){
        Random r = new Random();
        return r.nextInt(end - start + 1) + start;
    }

}
