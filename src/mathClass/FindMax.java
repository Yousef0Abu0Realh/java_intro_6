package mathClass;

public class FindMax {
    public static void main(String[] args) {

        int num1 = -10, num2 = -15;

        int max = Math.max(num1, num2);

        System.out.println(max);

        int number1 = 2;
        int number2 = 35;
        int number3 = 5;
        int number4 = 18;

        int max2 = Math.max(number1,Math.max(number2,Math.max(number3,number4)));

        System.out.println(max2);

        number1 = -30;
        number2 = -40;
        number3 =  0;

        max = Math.max(number1,number2);

        System.out.println(Math.max(max,number3));

        int a = 5, b = 10, c = 50, d = 189, f = 12,finalMax = Math.max(Math.max(Math.max(a,b),Math.max(c,d)),f);

        System.out.println(finalMax);



    }
}
