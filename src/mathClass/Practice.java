package mathClass;

import java.util.Scanner;

public class Practice {
    public static void main(String[] args) {

        System.out.println("\n task 01\n");

        int random1 = (int)(Math.round(Math.random()*50));

        System.out.println(random1);

        System.out.println("The random number * 5 = {"+(5*(random1)+"}"));

        System.out.println("\n task 02\n");

        int random2 = (int)(Math.round(Math.random()*9)+1);

        int random3 = (int)(Math.round(Math.random()*9)+1);

        int min = Math.min(random2,random3);

        int max = Math.max(random2,random3);

        int diff = Math.abs(random2 - random3);

        System.out.println(random2);
        System.out.println(random3);

        System.out.println("Min number = " + min);
        System.out.println("Max number = " + max);
        System.out.println("Difference = " + diff);

        System.out.println("\n task 03\n");

        int random4 = (int)(Math.round(Math.random()*50)+50);

        System.out.println("the random number = " + random4);

        System.out.println("The random number % 10 = " + (random4%10));

        System.out.println("\n task 04\n");

        Scanner input = new Scanner(System.in);

        System.out.println("Hey user can you give me 5 numbers");

        int num1 = input.nextInt();
        int num2 = input.nextInt();
        int num3 = input.nextInt();
        int num4 = input.nextInt();
        int num5 = input.nextInt();

        int result =(num1*5)+(num2*4)+(num3*3)+(num4*2)+(num5*1);

        System.out.println(result);




    }
}
