package MockInterviewPractice;

import java.sql.SQLOutput;

public class Practice01 {
    public static void main(String[] args) {
       task01("a");
    }

    public static void task01(String str){

        if (str.length() > 0){
            System.out.println("The length is = " + str.length());
            System.out.println("The first char " + str.charAt(0));
            System.out.println("The last char " + str.charAt(str.length()-1));
            if(str.toLowerCase().contains("a") || str.toLowerCase().contains("e") || str.toLowerCase().contains("i")||str.toLowerCase().contains("o")||str.toLowerCase().contains("u")){
                System.out.println("This word contains vowels");
            }
            else System.out.println("This word has no vowels");
        }
        else{
            System.out.println("The length is less than 1");
        }

    }
}
