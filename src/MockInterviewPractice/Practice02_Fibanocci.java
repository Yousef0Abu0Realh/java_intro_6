package MockInterviewPractice;

import utilities.ScannerHelper;

import java.util.ArrayList;
import java.util.Arrays;

public class Practice02_Fibanocci {
    public static void main(String[] args) {

        System.out.println(fib(ScannerHelper.getnumber()));

    }

    public static String fib(int numb) {

        int[] arr = new int[numb];

        int first = 0, second = 1;

        for (int i = 0; i < numb; i++) {

            arr[i] = (first);

            int nextNum = first + second;

            first = second;

            second = nextNum;

        }
        return Arrays.toString(arr);
    }
}
