package strings;

public class Exercise01 {
    public static void main(String[] args) {
        /*
        String is a reference type (object) that is used to store a sequance of characters
        Text
         */
        String name = "Yousef";
        String address = "FAIRFAX";

        String favMovie = "No country for old men";
        System.out.println("My favoirte movie = {"+favMovie+"}" );
    }
}
