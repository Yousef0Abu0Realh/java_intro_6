package strings;

public class UnderstandingStrings {
    public static void main(String[] args) {
        String s1;

        s1= "Techglobal School";

        String s2 = "is the best";

        String s3 = s1+" "+s2;

        System.out.println(s3);

        String s4 = s1.concat(" ").concat(s2);
        System.out.println(s4);

        String wordPart1 = "le",wordPart2 = "ar", wordPart3 = "ning";

        String fullWord = wordPart1+wordPart2+wordPart3;

        System.out.println(fullWord);
    }
}
