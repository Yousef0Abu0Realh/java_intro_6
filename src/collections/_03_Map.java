package collections;

import java.util.*;

public class _03_Map {
    public static void main(String[] args) {
        /*
        Map is an interface, and it has some implementation classes
        1.HashMap
         */


        System.out.println("\n ========= HashMap ========== \n");

        HashMap<String, Integer> hashMap = new HashMap<>();

        hashMap.put("Yousef", 23);
        hashMap.put("Zel", 20);
        hashMap.put("Assem", 27);
        hashMap.put(null, 24);
        hashMap.put(null, null);
        hashMap.put(null, 20);
        hashMap.put(null, 13);

        System.out.println(hashMap);


        System.out.println("\n--- linkedHashMap---\n");

        LinkedHashMap<String,Integer> linkedHashMap = new LinkedHashMap<>();

        linkedHashMap.put("Yousef", 23);
        linkedHashMap.put("Zel", 20);
        linkedHashMap.put("Assem", 27);
        linkedHashMap.put(null, 24);
        linkedHashMap.put(null, null);
        linkedHashMap.put(null, 20);
        linkedHashMap.put(null, 13);

        System.out.println(linkedHashMap);




        System.out.println("\n--- TreeMap---\n");

        TreeMap<String,Integer> treeMap = new TreeMap<>();

        treeMap.put("Yousef", 23);
        treeMap.put("Zel", 20);
        treeMap.put("Assem", 27);

        System.out.println(treeMap);


        System.out.println("\n --- hashtable --- \n");

        Hashtable<String,Integer> hashtable = new Hashtable<>();

        hashtable.put("Adam",20);
        hashtable.put(null,20);
    }
}
