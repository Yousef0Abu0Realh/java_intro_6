package collections;

import java.util.*;

public class _02_Set {
            public static void main(String[] args) {

                /*
        Set is an interface, and it has some class implementation as below
            -Set does not keep insertion order
            -Set does not allow duplicates
            -Set allow ONLY 1 null element

        1. HashSet: the most common Set implementation
               - It does not keep insertion order
               - It does not allow duplicates
               - It allows ONLY 1 null element

        2. LinkedHashSet
               -
        3. TreeSet
         */

                System.out.println("\n ---------- hashSet -------- \n");

                HashSet<String> objects = new HashSet<>();
                objects.add(null);
                objects.add("Sandina");
                objects.add(null);
                objects.add("Okan");
                objects.add("Alex");
                objects.add("Alex");
                objects.add("John");
                objects.add("abc");
                objects.add("123");
                objects.add("");
                objects.add("Sal");
                objects.add("Boo");


                System.out.println(objects); // [null, Sandina, , Alex, 123, abc, Boo, John, Okan, Sal]

                ArrayList<String> list = new ArrayList<>(objects);

                System.out.println(list.get(1));


                objects.remove(null);

                System.out.println(objects);

                objects.add(null);

                System.out.println(objects);

                System.out.println("\n ---------- LinkedHashSet -------- \n");


                LinkedHashSet<String> words = new LinkedHashSet<>();

                words.add(null);
                words.add("Sandina");
                words.add(null);
                words.add("Okan");
                words.add("Alex");
                words.add("Alex");
                words.add("John");
                words.add("abc");
                words.add("123");
                words.add("");
                words.add("Sal");
                words.add("Boo");

                System.out.println(words);

                System.out.println("\n-------- tree set -------\n");
                TreeSet<String> treeSet = new TreeSet<>();

                
                treeSet.add("Sandina");
                treeSet.add("Okan");
                treeSet.add("Alex");
                treeSet.add("Alex");
                treeSet.add("John");
                treeSet.add("abc");
                treeSet.add("123");
                treeSet.add("");
                treeSet.add("Sal");
                treeSet.add("Boo");
                //treeSet.add(null);

                System.out.println(treeSet);

                System.out.println(treeSet.descendingSet());

                System.out.println(treeSet.subSet("Boo","Sandina"));


                System.out.println(treeSet.tailSet("Okan"));
                System.out.println(treeSet.headSet("Okan"));

                System.out.println(treeSet.floor("Okan"));
                System.out.println(treeSet.ceiling("Okan"));

                System.out.println(treeSet.lower("Okan"));
                System.out.println(treeSet.higher("Okan"));

                System.out.println(treeSet.first());
                System.out.println(treeSet.last());


                System.out.println("\n------------- HashSet, LinkedList,and TreeSet in the shape of set");

                List<Integer> numbers = new ArrayList<>();

            }
        }
