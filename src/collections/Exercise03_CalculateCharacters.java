package collections;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

public class Exercise03_CalculateCharacters {
    public static void main(String[] args) {

        System.out.println("\n ---- Task-1 ---- \n");
        String str = "banana";

        HashMap<Character,Integer> lettersMap = new HashMap();


        int num = 0;

        for (char letters : str.toCharArray()) {

            if(!lettersMap.containsKey(letters))lettersMap.put(letters, 1);
            else lettersMap.put(letters,lettersMap.get(letters)+1);
        }
        System.out.println(lettersMap);




        String word = "pineapple";

        Map<Character, Integer> characters = new HashMap<>();

        for (int i = 0; i < word.length(); i++) {
            char currentchar = word.charAt(i);

            if(characters.containsKey(currentchar)) characters.put(currentchar,characters.get(currentchar)+1);
            else characters.put(currentchar,1);

        }

        System.out.println(characters);

        System.out.println(characters.values());

        int max = new TreeSet<>(characters.values()).last();

        System.out.println(max);
    }
}
