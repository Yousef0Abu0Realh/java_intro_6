package collections;

import java.util.*;

public class Exercise01_RemoveDuplicates {
    public static void main(String[] args) {

        List<Integer> list1 = new ArrayList<>(Arrays.asList(2,3,4,5,5,2));
        List<Integer> list2 = new ArrayList<>(Arrays.asList(10,10,10,10,10));
        List<Integer> list3 = new ArrayList<>(Arrays.asList(-3,-3,-5,0,0));

        System.out.println(removeDuplicates(list1));
        System.out.println(removeDuplicates(list2));
        System.out.println(removeDuplicates(list3));

    }

    public static HashSet<Integer> removeDuplicates(List<Integer> numbs){

        return new HashSet<>(numbs);
    }

    public static TreeSet<Integer> removeDuplicates2(List<Integer> numbs){

        return new TreeSet<>(numbs);
    }



}
