package collections;

import java.util.Arrays;
import java.util.TreeSet;

public class Exercise02_FindMinMax {
    public static void main(String[] args) {

    }

    public static int max(Integer[] number){

        return new TreeSet<>(Arrays.asList(number)).last();
    }



    public static int min(Integer[] number){

        return new TreeSet<>(Arrays.asList(number)).first();
    }

    public static int secondmax(Integer[] number){

        return new TreeSet<>(Arrays.asList(number)).last();
    }



    public static int secondmin(Integer[] number){

        return new TreeSet<>(Arrays.asList(number)).first();
    }

}
