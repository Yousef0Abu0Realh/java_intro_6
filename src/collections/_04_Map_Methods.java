package collections;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class _04_Map_Methods {
    public static void main(String[] args) {

        HashMap<String,String> capitals = new HashMap<>();


        System.out.println("\n---- how to add entries ----\n");
        capitals.put("France","Paris");
        capitals.put("Italy","Rome");
        capitals.put("Spain","Madrid");

        System.out.println("\n ---- how to print a map ---- \n");
        System.out.println(capitals);


        System.out.println("\n ---- how to retrieve a value ----\n");
        System.out.println(capitals.get("France"));
        System.out.println(capitals.get("Italy"));
        System.out.println(capitals.get("Spain"));


        System.out.println(capitals.get("USA"));
        System.out.println(capitals.get("france"));

        System.out.println(capitals.containsKey("Italy"));
        System.out.println(capitals.containsKey("Belgium"));

        System.out.println(capitals.containsKey("Paris"));
        System.out.println(capitals.containsKey("Rome"));


        System.out.println(capitals.remove("USA"));
        System.out.println(capitals.remove("Italy","Rome"));

        System.out.println(capitals);


        capitals.put("Ukraine","Kviv");
        capitals.put("Turkey","Ankara");

        System.out.println(capitals.keySet());


        System.out.println(capitals.values());

        System.out.println(capitals.entrySet());


    }

}
