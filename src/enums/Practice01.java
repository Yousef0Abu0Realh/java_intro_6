package enums;

public class Practice01 {
    public static void main(String[] args) {


        DaysOfTheWeek dayByUser = DaysOfTheWeek.SUNDAY;

        switch (dayByUser){
            case SATURDAY:
            case SUNDAY:
                System.out.println("it is and off day");
                break;
            case MONDAY:
            case TUESDAY:
            case WEDNESDAY:
            case THURSDAY:
            case FRIDAY:
                System.out.println("it is a work day");
                break;
            default:
                throw  new RuntimeException("No such enum value !!!");
        }
    }
}
