package enums;

import java.time.DayOfWeek;
import java.time.Month;
import java.util.Arrays;

public class PracticeEnums {
    public static void main(String[] args) {
        System.out.println(DaysOfTheWeek.SATURDAY);

        for (DaysOfTheWeek day : DaysOfTheWeek.values()) {
            System.out.println(day);
        }

        System.out.println(Arrays.toString(Month.values()));
    }
}
