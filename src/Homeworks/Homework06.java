package Homeworks;

import java.util.Arrays;

public class Homework06 {
    public static void main(String[] args) {
        System.out.println("\n --- task 01 --- \n");

        int[] numbers = new int[10];

        numbers[2] = 23;
        numbers[4] = 12;
        numbers[7] = 34;
        numbers[9] = 7;
        numbers[6] = 15;
        numbers[0] = 89;

        System.out.println(numbers[3]);
        System.out.println(numbers[0]);
        System.out.println(numbers[9]);
        System.out.println(Arrays.toString(numbers));

        System.out.println("\n --- task 02 --- \n");

        String[] letters = new String[5];

        letters[1] = "abc";
        letters[4] = "xyz";

        System.out.println(letters[3]);
        System.out.println(letters[0]);
        System.out.println(letters[4]);
        System.out.println(Arrays.toString(letters));

        System.out.println("\n --- task 03 --- \n");

        int[] numbers2 = {23, -34, -56, 0, 89, 100};

        System.out.println(Arrays.toString(numbers2));

        Arrays.sort(numbers2);

        System.out.println(Arrays.toString(numbers2));

        System.out.println("\n --- task 04 --- \n");

        String[] countries = {"Germany", "Argentina", "Ukraine", "Romania"};

        System.out.println(Arrays.toString(countries));

        Arrays.sort(countries);

        System.out.println(Arrays.toString(countries));


        System.out.println("\n --- task 05 --- \n");

        String[] cartoonDogs = {"Scooby Doo", "Snoopy", "Blue", "Pluto", "Dino", "Sparky"};

        System.out.println(Arrays.toString(cartoonDogs));

        boolean containsDog = false;

        for (String cartoonDog : cartoonDogs) {

            if (cartoonDog.equals("Pluto")){
                containsDog = true;
                break;
            }
        }

        System.out.println(containsDog);


        System.out.println("\n --- task 06 --- \n");

        String[] cartoonCats = {"Garfield", "Tom", "Sylvester", "Azrael"};

        Arrays.sort(cartoonCats);

        System.out.println(Arrays.toString(cartoonCats));

        boolean containsCat = false;

        for (String cartoonCat : cartoonCats) {

            if(cartoonCat.equals("Garfield") && cartoonCat.equals("Felix")) {

                containsCat = true;
                break;
            }
        }

        System.out.println(containsCat);


        System.out.println("\n --- task 07 --- \n");

        double[] numbers3 = {10.5, 20.75, 70, 80, 15.75};

        System.out.println(Arrays.toString(numbers3));

        for (double v : numbers3) {

            System.out.println(v);

        }

        System.out.println("\n --- task 08 --- \n");

        char[] characters = {'A', 'b', 'G', 'H', '7', '5', '&', '*', 'e', '@', '4'};

        System.out.println(Arrays.toString(characters));

        int letter = 0 , upper = 0 , lower = 0 , digits = 0 , specialC = 0;

        for (char character : characters) {

            if(Character.isLetter(character)){
                letter++;
                if(Character.isUpperCase(character)) upper++;
                else lower++;
            }
            else if (Character.isDigit(character)) digits++;
            else specialC++;
        }

        System.out.println("Letters = " + letter);
        System.out.println("Uppercase letters = " + upper);
        System.out.println("Lowercase letters = " + lower);
        System.out.println("Digits = " + digits);
        System.out.println("Special characters = " + specialC);


        System.out.println("\n --- task 09 --- \n");

        String[] schoolObjects = {"Pen", "notebook", "Book", "paper", "bag", "pencil", "Ruler"};

        System.out.println(Arrays.toString(schoolObjects));

        int upperC = 0, lowerC = 0, startsWithBorP = 0, containsPenOrBook = 0;

        for (String schoolObject : schoolObjects) {

            if (Character.isUpperCase(schoolObject.charAt(0)))upperC++;
            else if (Character.isLowerCase(schoolObject.charAt(0)))lowerC++;
            if ((schoolObject.toLowerCase().charAt(0) == 'b') || (schoolObject.toLowerCase().charAt(0) == 'p'))startsWithBorP++;
            if (schoolObject.toLowerCase().contains("book") || schoolObject.toLowerCase().contains("pen")) containsPenOrBook++;

        }

        System.out.println("Elements starts with uppercase = " + upperC);
        System.out.println("Elements starts with lowercase = " + lowerC);
        System.out.println("Elements starting with B or P = " + startsWithBorP);
        System.out.println("Element having \"book\" or \"pen\" = "+ containsPenOrBook);


        System.out.println("\n --- task 10 --- \n");

        int[] numbers4 = {3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78};

        System.out.println(Arrays.toString(numbers4));

        int moreThan = 0, lessThan = 0, equals =0;

        for (int i : numbers4) {

            if(i > 10)moreThan++;
            else if (i < 10)lessThan++;
            else if (i == 10)equals++;
        }

        System.out.println("Elements that are more than 10 = " + moreThan);
        System.out.println("Elements that are less than 10 = " + lessThan);
        System.out.println("Elements that are 10 = " + equals);


        System.out.println("\n --- task 11 --- \n");

        int[] array1 = {5, 8, 13, 1, 2};
        int[] array2 = {9, 3, 67, 1, 0};
        int[] max = new int[5];

        for (int i = 0; i < max.length; i++) {

            max[i] = Math.max(array1[i],array2[i]);
        }

        System.out.println("1st array is = " + Arrays.toString(array1));
        System.out.println("2nd array is = " + Arrays.toString(array2));
        System.out.println("3rd array is = " + Arrays.toString(max));

    }
}
