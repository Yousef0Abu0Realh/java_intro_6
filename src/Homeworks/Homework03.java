package Homeworks;

import java.util.Scanner;

public class Homework03 {
    public static void main(String[] args){
        System.out.println("\n----task_1----\n");
        Scanner num = new Scanner(System.in);
        System.out.println("Hey user can you give me a number");
        int Num1 = num.nextInt();
        System.out.println("Hey user can you give me another number");
        int Num2 = num.nextInt();
        System.out.println("The Difference between the numbers is = " + Math.abs(Num1-Num2));

        System.out.println("\n----task_2----\n");

        System.out.println("Hey user can you give me a number?");
        int Num1a = num.nextInt();
        System.out.println("can you give me another number?");
        int Num2b = num.nextInt();
        System.out.println("can you give me another number?");
        int Num3c = num.nextInt();
        System.out.println("can you give me another number?");
        int Num4d = num.nextInt();
        System.out.println("can you give me another number?");
        int Num5e = num.nextInt();

        int minv1 = Math.min(Num1a,Num2b), minv2 = Math.min(Num3c,Num4d), minv3 =Math.min(minv1,minv2), MIN = Math.min(minv3,Num5e);
        int maxv1 = Math.max(Num1a,Num2b), maxv2 = Math.max(Num3c,Num4d), maxv3 =Math.max(maxv1,maxv2), MAX = Math.max(maxv3,Num5e);

        System.out.println("Max value of the numbers given is = " + MAX);
        System.out.println("Min value of the numbers given is = " + MIN);

        System.out.println("\n----task_3----\n");

        int randomnum1 = (int)(Math.random() * (100 - 50 +1) + 50);
        int randomnum2 = (int)(Math.random() * (100 - 50 +1) + 50);
        int randomnum3 = (int)(Math.random() * (100 - 50 +1) + 50);
        int random = (int)(randomnum1+randomnum2), randomSum = (int)(random+randomnum3);

        System.out.println("Number 1 = " + randomnum1);
        System.out.println("Number 2 = " + randomnum2);
        System.out.println("Number 3 = " + randomnum3);
        System.out.println("The sum of the numbers is = " + randomSum);

        System.out.println("\n------task_4------\n");
        double AM = 125, MM = 220;
        System.out.println("Alex's money: $" + (AM-25.5));
        System.out.println("Mike's money: $" + (MM+25.5));

        System.out.println("\n ---- task_5----\n");
        Scanner input = new Scanner (System.in);
        double priceForbicycle = 390;

        System.out.println("The price of the Bicycle is currently = $" + priceForbicycle);
        System.out.println("How much are you saving daily?");
        double dailySave = input.nextDouble();

        System.out.println("You can buy a Bicycle in " + (int)(priceForbicycle/ dailySave) + " days.");

        System.out.println("\n---task_6---\n");
        String s1 = "5",s2 = "10";
        int s3 = Integer.parseInt(s1),s4 = Integer.parseInt(s2);

        System.out.println("sum of 5 and 10 is = " + (s3+s4));
        System.out.println("product of 5 and 10 is = " + (s3*s4));
        System.out.println("Division of 5 and 10 is = " + (s3/s4));
        System.out.println("Subtraction of 5 and 10 is = " + (s3-s4));
        System.out.println("Remainder of 5 and 10 is = " + (s3%s4));

        System.out.println("\n---task_7---\n");

        String a1 = "200",a2 = "-50";
        int a3 = Integer.parseInt(a1),a4 = Integer.parseInt(a2);

        System.out.println("The Greatest value is = " + Math.max(a3,a4));
        System.out.println("The Smallest value is = " + Math.min(a3,a4));
        System.out.println("The Average is = " + (a3+a4)/2);
        System.out.println("The absolute difference is = " + Math.abs(a4-a3));

        System.out.println("\n---task_8---\n");
        double coin = 0.96;

        System.out.println((int)(24/coin)+ " Days");
        System.out.println((int)(168/coin) + " Days");
        System.out.println("$" + ((5*30)*(coin)));

        System.out.println("\n---task_9---\n");

        double priceForComputer = 1250, Savings = 62.5;

        System.out.println("It will take Jessie "+(int)(priceForComputer/Savings) +"Days");



        System.out.println("\n---task_10---\n");
        double car = 14265,option1 = 475.50,option2 = 951;

        System.out.println("Option 1 will take " + (int)(car/option1) + " Months");
        System.out.println("Option 1 will take " + (int)(car/option2) + " Months");


        System.out.println("\n---task_11---\n");

        Scanner num1 = new Scanner(System.in);

        System.out.println("Hey user can you give me a number");
        int Var = num1.nextInt();
        System.out.println("Hey user can you give me another number");
        int Var2 = num1.nextInt();

        double v1 = Double.valueOf(Var);
        double v2 = Double.valueOf(Var2);

        double Var3 =(v1/v2);

        System.out.println(Var3);


        System.out.println("\n-----task12-----");

        int randomnum4 = (int)(Math.random() * (100 - 0 +1));
        int randomnum5 = (int)(Math.random() * (100 - 0 +1));
        int randomnum6 = (int)(Math.random() * (100 - 0 +1));

        System.out.println("Random 1 number =" + randomnum4);
        System.out.println("Random 2 number =" + randomnum5);
        System.out.println("Random 3 number =" + randomnum6);
        System.out.println("\nAre all numbers greater than or equal to 25?\n");

        if((randomnum4 >= 25)&&(randomnum5 >= 25)&&(randomnum6 >= 25)) {
            System.out.println(true);
        }
        else{
            System.out.println(false);
        }

        System.out.println("\n---task13---");
        Scanner day = new Scanner(System.in);
        System.out.println("Hey user tell me what day of the week it is with (1 = monday) - (7 = sunday)");
        int day0 = day.nextInt();
        if (day0 == 1) System.out.println("Monday");
        else if (day0 == 2) System.out.println("Tuesday");
        else if (day0 == 3) System.out.println("Wednesday");
        else if (day0 == 4) System.out.println("Thursday ");
        else if (day0 == 5) System.out.println("Friday ");
        else if (day0 == 6) System.out.println("Saturday");
        else if (day0 == 7)System.out.println("Sunday");
        else System.out.println("this is not a valid entery");

        System.out.println("\n---task14---");
        Scanner input1 = new Scanner(System.in);

        System.out.println("Hey user! please enter your first math test grade");
        int examResult1 = input1.nextInt();
        System.out.println("please enter your second math test grade");
        int examResult2 = input1.nextInt();
        System.out.println("please enter your third math test grade");
        int examResult3 = input1.nextInt();

        int examResultf = (((examResult1 + examResult2) + examResult3) / 3);

        if(examResultf>= 70){
            System.out.println("YOU PASSED!");
        }
        else {
            System.out.println("YOU FAILED");
        }

        System.out.println("\n------task 15----\n");

        Scanner Match = new Scanner(System.in);
        System.out.println("hey user can you give me a number");
        int nm = Match.nextInt();
        System.out.println("hey user can you give me another number");
        int nm2 = Match.nextInt();
        System.out.println("hey user can you give me another number");
        int nm3 = Match.nextInt();


        if(nm==nm2 && nm2 == nm3 && nm == nm3){
            System.out.println("Triple match");
        }
        else if ((nm != nm2) && (nm2 != nm3) && (nm != nm3)) {
            System.out.println("No match");
        }
        else if ((nm != nm2) || (nm != nm3) || (nm2 != nm3) ||(nm == nm2) || (nm2 == nm3) || (nm == nm3)) {
            System.out.println("double Match");


        }
    }
}
