package Homeworks;

import java.util.Scanner;

public class Homework02 {
    public static void main(String[] args) {

        System.out.println("\n=======Task 01 =======\n");

        Scanner input = new Scanner(System.in);

        System.out.println("Hey user can you give a number");

        int num1 = input.nextInt();

        System.out.println("now give me a second number");

        int num2 = input.nextInt();

        System.out.println("The number 1 entered by the user is = " + num1);

        System.out.println("The number 2 entered by the user is = " + num2);

        System.out.println("The sum of the number 1 and 2 entered by user is = " +(num1+num2));

        System.out.println("\n=======Task 02 =======\n");

        System.out.println("Hey user can you give a number");

        int num3 = input.nextInt();

        System.out.println("Now give me another number");

        int num4 = input.nextInt();

        System.out.println("The Product of the given 2 numbers is : " + (num3*num4));

        System.out.println("\n=======Task 03 =======\n");

        System.out.println("Hey user enter floating numbers");

        double d1 = input.nextDouble();

        System.out.println("Now give another floating number");

        double d2 = input.nextDouble();

        System.out.println("The sum of the given numbers is = " + (d1+d2));
        System.out.println("The product of the given numbers is = " + (d1*d2));
        System.out.println("The subtraction of the given numbers is = " + (d1-d2));
        System.out.println("The division of the given numbers is = " + (d1/d2));
        System.out.println("The remainder of the given numbers is = " + (d1%d2));

        System.out.println("\n=======Task 04 =======\n");

        System.out.println("1.\t\t" + (-10 + 7 * 5));
        System.out.println("2.\t\t" + ((72+24)%24));
        System.out.println("3.\t\t" + (10 + -3*9/9));
        System.out.println("4.\t\t" + (5+ 18/3*3-(6%3)));

        System.out.println("\n=======Task 05 =======\n");

        System.out.println("Hey user can you give me a number");

        int n1 = input.nextInt();

        System.out.println("Now give me another number");

        int n2 = input.nextInt();

        System.out.println("The average of the given numbers is: " + ((n1+n2)/2));

        System.out.println("\n=======Task 06 =======\n");

        System.out.println("hey user give 5 numbers");
        int a1 = input.nextInt();
        int a2 = input.nextInt();
        int a3 = input.nextInt();
        int a4 = input.nextInt();
        int a5 = input.nextInt();

        System.out.println("the average of the given numbers is: " +((a1+a2+a3+a4+a5)/5));

        System.out.println("\n=======Task 07 =======\n");

        System.out.println("Hey user give me 3 different numbers");

        int f1 = input.nextInt();
        int f2 = input.nextInt();
        int f3 = input.nextInt();

        System.out.println("The " +f1+ " multipled with " +f1+ " is = "+(f1*f1));
        System.out.println("The " +f2+ " multipled with " +f2+ " is = "+(f2*f2));
        System.out.println("The " +f3+ " multipled with " +f3+ " is = "+(f3*f3));

        System.out.println("\n=======Task 08 =======\n");

        System.out.println("Hey user tell me the length of the side of a square");

        int l1 = input.nextInt();

        System.out.println("Perimeter of the square = "+(4*l1));
        System.out.println("area of the square = "+(l1*l1));

        System.out.println("\n=======Task 09 =======\n");

        System.out.println("A Software Engineer in Test can earn $" +(90000*3) + " in 3 years." );

        System.out.println("\n=======Task 10 =======\n");

        System.out.println("Hey user, can you tell me your favorite book.");

        String favBook = input.nextLine();

        System.out.println("Now tell me your favorite color.");

        String favColor = input.next();

        System.out.println("Now tell me your favorite number");

        int favNumber = input.nextInt();


        System.out.println("User's favorite book is: " + favBook + "\nUser's favorite color: " + favColor + "\nUser's favorite number is: " +favNumber);


        System.out.println("\n=======Task 11 =======\n");

        System.out.println("Hey user, can you tell me your First name");

        String fName = input.nextLine();

        System.out.println("Now tell me your Last name");

        String lName = input.nextLine();

        System.out.println("Now tell me your age");

        int age = input.nextInt();

        System.out.println("Now tell me your email address");

        String email = input.nextLine();

        System.out.println("Now tell me your phone number");

        int phoneNumber = input.nextInt();

        System.out.println("Now tell me your address");

        String address = input.nextLine();

        System.out.println(" User who joined this program is " + fName + " " + lName + ". " + fName + "’s age is" + age + ". John’s email address is " + email +", phone number is " + phoneNumber + ", and address is"+ address + ".");




    }
}
