package Homeworks;

public class Homework01 {
    public static void main(String[] args) {

        System.out.println("\n----Task 01----\n");

        /*
        JAVA- 01001010 01000001 01010110 01000001

        Selenium- 01010011 01100101 01101100 01100101 01101110 01101001 01110101 01101101
         */

        System.out.println("\n----Task 02----\n");

        /*
        01001101- M
        01101000- h
        01111001- y
        01010011- S
        01101100- l
         */

        System.out.println("\n----Task 03----\n");

        System.out.println("I started practicing \"JAVA\" today, and I like it.");

        System.out.println("The secret of getting ahead is getting started");

        System.out.println("\"Don't limit yourself\"");

        System.out.println("Invest in your dreams. Grind now. Shine later.");

        System.out.println("It’s not the load that breaks you down, it’s the way you carry it.");

        System.out.println("The hard days are what make you stronger.");

        System.out.println("You can waste your live drawing lines. or you can live your life crossing them.");

        System.out.println("\n----Task 04----\n");

        System.out.println("\tJava is easy to write and easy to run—this is the\n foundational strength of Java and why many developers\n program in it. When you write Java once, you can run it\n almost anywhere at any time.\n\n\n\tJava can be used to create complete applications\nthat can run on a single computer or be distributed\nacross servers and clients in a network.\n\n\n\tAs a result, you can use it to easily build mobile \napplications or run-on desktop applications that use\ndifferent operating systems and servers, such as Linux \nor Windows.");

        System.out.println("\n----Task 05----\n");

        byte myAge = 22;
        int myFavoriteNumber = 2000000;
        double myHeight = 5.9;
        int myWeight = 250;
        char myFavoriteLetter = 'Y';

        System.out.println("My age is = " + myAge);
        System.out.println("My favorite number is = " + myFavoriteNumber);
        System.out.println("My height is = " + myHeight);
        System.out.println("My weight is = " + myWeight);
        System.out.println("My favorite letter is = " + myFavoriteLetter);


    }
}
