package Homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Homework07 {
    public static void main(String[] args) {

        System.out.println("\n--- Task 01 ---\n");

        ArrayList<Integer> nums = new ArrayList<>(Arrays.asList(10, 23, 67, 23, 78));

        System.out.println(nums.get(3));
        System.out.println(nums.get(0));
        System.out.println(nums.get(2));
        System.out.println(nums);

        System.out.println("\n--- Task 02 ---\n");

        ArrayList<String> colors = new ArrayList<>(Arrays.asList("Blue", "Brown", "Red", "White", "Black", "Purple"));

        System.out.println(colors.get(1));
        System.out.println(colors.get(3));
        System.out.println(colors.get(5));

        System.out.println(colors);

        System.out.println("\n--- Task 03 ---\n");

        ArrayList<Integer> nums2 = new ArrayList<>(Arrays.asList(23, -34, -56, 0, 89, 100));

        System.out.println(nums2);

        Collections.sort(nums2);

        System.out.println(nums2);


        System.out.println("\n--- Task 04 ---\n");

        ArrayList<String> cities = new ArrayList<>(Arrays.asList("Istanbul", "Berlin", "Madrid", "Paris"));

        System.out.println(cities);

        Collections.sort(cities);

        System.out.println(cities);


        System.out.println("\n--- Task 05 ---\n");

        ArrayList<String> marvelSuperHeros = new ArrayList<>(Arrays.asList("Spider Man", "Iron Man", "Black Panther", "Deadpool", "Captain America"));

        System.out.println(marvelSuperHeros);

        System.out.println(marvelSuperHeros.contains("Wolverine"));

        System.out.println("\n--- Task 06 ---\n");

        ArrayList<String> avengers = new ArrayList<>(Arrays.asList("Hulk", "Black Widow", "Captain America", "Iron Man"));

        Collections.sort(avengers);

        System.out.println(avengers);

        System.out.println(avengers.contains("Hulk") && avengers.contains("Iron Man"));


        System.out.println("\n--- Task 07 ---\n");

        ArrayList<Character> chars = new ArrayList<>(Arrays.asList('A', 'x', '$', '%', '9', '*', '+', 'F', 'G'));

        System.out.println(chars);

        for (Character aChar : chars) {
            System.out.println(aChar);
        }

        System.out.println("\n--- Task 08 ---\n");

        ArrayList<String> objects = new ArrayList<>(Arrays.asList("Desk", "Laptop", "Mouse", "Monitor", "Mouse-pad", "Adapter"));

        System.out.println(objects);

        Collections.sort(objects);

        System.out.println(objects);

        int startsWithM = 0;

        int noA = objects.size();

        for (String object : objects) {

            if (object.toUpperCase().startsWith("M"))startsWithM++;
            if (object.toUpperCase().contains("A") || object.toUpperCase().contains("E"))noA--;

        }

        System.out.println(startsWithM);
        System.out.println(noA);

        System.out.println("\n--- Task 09 ---\n");

        ArrayList<String> kitchenObjects = new ArrayList<>(Arrays.asList("Plate", "spoon", "fork", "Knife", "cup", "Mixer"));

        int startsWithUpper = 0, startsWithLower = 0, hasP = 0, startsOrEndsWithP = 0;

        for (String kitchenObject : kitchenObjects) {

            if(Character.isUpperCase(kitchenObject.charAt(0))) startsWithUpper++;
            else if (Character.isLowerCase(kitchenObject.charAt(0))) startsWithLower++;
            if(kitchenObject.toLowerCase().contains("p")) hasP++;
            if ((kitchenObject.toLowerCase().startsWith("p") || kitchenObject.toLowerCase().endsWith("p"))) startsOrEndsWithP++;
        }
        System.out.println("Element starts with uppercase = " + startsWithUpper);
        System.out.println("Element starts with lowercase = " + startsWithLower);
        System.out.println("Element having P or p = " + hasP);
        System.out.println("Element starting or ending with P or p = " + startsOrEndsWithP);


        System.out.println("\n--- Task 10 ---\n");

        ArrayList<Integer> num3 = new ArrayList<>(Arrays.asList(3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78));

        int divide10 = 0, evenGreaterThan15 = 0, oddAndLessThan20 = 0, lessThan15OrGreaterThan50 = 0;

        for (Integer integer : num3) {

            if(integer % 10 == 0) divide10++;
            if(integer % 2 == 0 && integer > 15) evenGreaterThan15++;
            if(integer % 2 == 1 && integer < 20) oddAndLessThan20++;
            if(integer < 15 || integer > 50) lessThan15OrGreaterThan50++;

        }

        System.out.println("Element that can be divided by 10 = " + divide10);
        System.out.println("Element that are even and are greater than 15 = " + evenGreaterThan15);
        System.out.println("Element that are odd and are less than 20 = " + oddAndLessThan20);
        System.out.println("Element that are less than 15 or greater than 50 = " + lessThan15OrGreaterThan50);






    }
}
