package Homeworks;

import utilities.ScannerHelper;

public class Homework04 {


    public static void main(String[] args) {

        System.out.println("\n **** task 01 **** \n");

        String firstname = ScannerHelper.getFirstName();

        System.out.println("The length of the name is = " + firstname.length());
        System.out.println("The first character in the name is = " + firstname.charAt(0));
        System.out.println("The last Character in the name is  = " + firstname.charAt(firstname.length()-1));
        System.out.println("The First three letters of the name is  = " + firstname.substring(0,3));
        System.out.println("The last three letters of the name is  = " + firstname.substring(firstname.length()-3,firstname.length()-0));
        System.out.println((firstname.toUpperCase().startsWith("A"))?("You're in the club"):("Sorry,you are not in the club"));

        System.out.println("\n **** task 02 **** \n");

        String address = ScannerHelper.getAddress();
        if(address.toUpperCase().contains("CHICAGO")){System.out.println("Your are in the club");}
        else if (address.toUpperCase().contains("DES PLAINES")) {System.out.println("You're welcome to join the club");}
        else{System.out.println("Sorry,You will never be in the club");}

        System.out.println("\n **** task 03 **** \n");

        System.out.println("Hey user, tell me your favorite country.");

        String favCountry = ScannerHelper.getString();

        if (favCountry.toUpperCase().contains("A")){
            if(favCountry.toUpperCase().contains("I")) System.out.println("A and i are there");
            else System.out.println("A is there");}
        else if(favCountry.toUpperCase().contains("I")){System.out.println("I is there");}
        else{System.out.println("A and i are not there");}

        System.out.println("\n **** task 04 **** \n");

        String str = "Java is fun";

        System.out.println("The first word in the str is = " + str.toLowerCase().substring(0,4));
        System.out.println("The second word in the str is = " + str.toLowerCase().substring(5,7));
        System.out.println("The third word in the str is = " + str.toLowerCase().substring(8,11));



    }
}
