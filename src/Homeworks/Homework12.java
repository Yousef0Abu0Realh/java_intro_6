package Homeworks;

import java.util.ArrayList;
import java.util.Arrays;

public class Homework12 {
    public static void main(String[] args) {

        System.out.println("--- Task 01 ---");
        System.out.println(noDigits("123Hello"));
        System.out.println(noDigits("123Hello World149"));
        System.out.println(noDigits("123Tech456Global149"));
        System.out.println("--- Task 02 ---");
        System.out.println(noVowels("xyz"));
        System.out.println(noVowels("JAVA"));
        System.out.println(noVowels("123$"));
        System.out.println(noVowels("TechGlobal"));

        System.out.println("--- Task 03 ---");

        System.out.println(sumOfDigits(""));
        System.out.println(sumOfDigits("Java"));
        System.out.println(sumOfDigits("John's age is 29"));
        System.out.println(sumOfDigits("$125.0"));

        System.out.println("--- Task 04 ---");

        System.out.println(hasUpperCase(""));
        System.out.println(hasUpperCase("java"));
        System.out.println(hasUpperCase("John's age is 29"));

        System.out.println("--- Task 05 ---");

        System.out.println(middleInt(new int[] {1,1,1}));
        System.out.println(middleInt(new int[] {1,2,2}));
        System.out.println(middleInt(new int[] {5,5,8}));

        System.out.println("--- Task 06 ---");

        System.out.println(Arrays.toString(no13(new int[]{1, 2, 3, 4})));
        System.out.println(Arrays.toString(no13(new int[]{13, 2, 3,})));
        System.out.println(Arrays.toString(no13(new int[]{13, 13, 13, 13, 13})));


        System.out.println("--- Task 07 ---");

        System.out.println(Arrays.toString(arrFactorial(new int[]{1, 2, 3, 4})));
        System.out.println(Arrays.toString(arrFactorial(new int[]{0, 5})));
        System.out.println(Arrays.toString(arrFactorial(new int[]{5,0,6})));


    }


    //Method 01
    public static String noDigits(String word){

        StringBuilder noDigs = new StringBuilder();
        StringBuilder digs = new StringBuilder();

        for (int i = 0; i <= word.length()-1 ; i++) {

            if(word.charAt(i) >= '0' &&  word.charAt(i) <= '9') digs.append(word.charAt(i));
            else noDigs.append(word.charAt(i));

        }

        return noDigs.toString();

    }

    //Method 02
   public static String noVowels(String word){

        StringBuilder noVowels = new StringBuilder();

        for (int i = 0; i < word.length(); i++) {

            if (word.toLowerCase().charAt(i) != 'a' && word.toLowerCase().charAt(i) != 'e'&& word.toLowerCase().charAt(i) != 'i' && word.toLowerCase().charAt(i) != 'o'&& word.toLowerCase().charAt(i) != 'u')noVowels.append(word.charAt(i));

        }
        return noVowels.toString();
    }

    //Method 03

    public static int sumOfDigits(String sentence){
        int sum = 0;
        char[] letters = sentence.toCharArray();
        ArrayList<Integer> numbs = new ArrayList<>();


        for (char letter : letters) {
            if(letter >= 0 &&  letter <= 9)numbs.add((int) letter);
        }
        for (Integer numb : numbs) {
            sum += numb;
        }
        return sum;
    }

    //Method 04

   public static boolean hasUpperCase(String sentence) {

       boolean isUpper = false;

       for (int i = 0; i <= sentence.length() - 1; i++) {

           if (sentence.charAt(i) == sentence.toUpperCase().charAt(i)) {
               isUpper = true;
               break;
           }
       }
       return isUpper;
   }


    //Method 05

    public static int middleInt(int[] num){

        Arrays.sort(num);

        return num[1];
    }

    //Method 06

    public static int[] no13(int[] numbs){

        int[] no13 = new int[numbs.length];

        for (int i = 0; i <= numbs.length - 1 ; i++) {

            if(numbs[i] == 13) no13[i] = 0;
            else no13[i] = numbs[i];
        }
        return no13;

    }

    public static int findFactorial(int num){

        int  factor = 1;
        for (int i = num; i >= 1  ; i--) {

            factor = factor * i;

        }

        return factor;
    }

    public static int[] arrFactorial(int[] arr){


        int[] factor = new int[arr.length];

        for (int i = 0; i <= arr.length ; i++) {

            factor[i] = findFactorial(arr[i]);
        }
        return factor;
    }

    public static String[] catagorizeCharacters(String str){

        return new String[]{str.replaceAll("[^a-zA-Z]",""),
                str.replaceAll("[^0-9]",""),
                str.replaceAll("[a-zA-Z0-9]","")};
    }
    



}
