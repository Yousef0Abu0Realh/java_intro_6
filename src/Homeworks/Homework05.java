package Homeworks;

import utilities.ScannerHelper;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Homework05 {
    public static void main(String[] args) {

        System.out.println("\n ----- Task 01 ----- \n");

        int count = 1;

        String num = "";

        while(count <= 100){

            if ( count == 7 && count % 7 == 0) num += count;

           else if (count % 7 == 0) num += " - " + count;

            count++;
        }
        System.out.println(num);

        System.out.println("\n ----- Task 02 ----- \n");

        String num2 = "";


        for (int i = 1; i <= 50 ; i++) {

            if (i == 2*3 ) num2 += i;
            else if(i % 2 == 0 && i % 3 == 0) num2 += " - " + i;
        }
        System.out.println(num2);

        System.out.println("\n ----- Task 03 ----- \n");

        String num3 = "";

        for (int i = 100; i >= 50 ; i--) {

            if ( i % 5 == 0 && i == 100) num3 += i;
            else if (i % 5 == 0) num3 += " - " + i;
        }
        System.out.println(num3);

        System.out.println("\n ----- Task 04 ----- \n");

        int count2 = 0;

        while ( count2 <= 7){
            System.out.println("The sqaure of " + count2 + " is = " + (count2*count2));
            count2++;
        }

        System.out.println("\n ----- Task 05 ----- \n");

        int num4 = 1;
        int num5 = 0;
        do{
            num5 += num4;
            num4++;
        }while(num4 <= 10);
        System.out.println(num5);

        System.out.println("\n ----- Task 06 ----- \n");

       int num6 = ScannerHelper.giveNumber();
       int num7 = 1;
       if (num6 == 0) System.out.println("The Factorial of 0 = 1 ");
       else{
           for (int i = 1 ; i <= num6 ; i++) {

               num7 = num7 * i;

           }
           System.out.println("The Factorial of " + num6 + " is  = " + num7);
       }
        System.out.println("\n ----- Task 07 ----- \n");

       Scanner input = new Scanner(System.in);

        System.out.println("Hey user can you give your fullname");

        String fullName = input.nextLine();

        System.out.println(fullName);

        int counter = 0;

        for (int i = 0; i < fullName.length(); i++) {

            fullName = fullName.toLowerCase();

            if(fullName.charAt(i) == 'a' || fullName.charAt(i) == 'e' || fullName.charAt(i) == 'i' || fullName.charAt(i) == 'o' || fullName.charAt(i) == 'u') counter++;

        }

        System.out.println("There are " + counter + " vowel letters in this full name");



        System.out.println("\n ----- Task 08 ----- \n");


        Scanner input1 = new Scanner(System.in);

        String name;

        do {

            System.out.println("Give me a name");

            name = input1.nextLine();



        }while(name.isEmpty() || name.charAt(0) != 'j' && name.charAt(0) !='J');
            System.out.println("End of program");


    }
}
