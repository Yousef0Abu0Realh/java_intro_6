package Homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;

public class Homework09 {

    public static void main(String[] args) {

        System.out.println("\n --- Task 01 --- \n");
        System.out.println(firstDuplicateNumber(new int[] {-4,0,-7,0,5,10,45,45}));

        System.out.println("\n --- Task 04 --- \n");
        System.out.println(duplicateStrings(new String[] {"A","foo","12","Foo","bar","a","a","Java"}));
        System.out.println(duplicateStrings(new String[] {"Python","foo","bar","java","123"}));

        System.out.println("\n --- Task 05 --- \n");
        System.out.println(reversedArray1(new String[] {"abc","foo","bar"}));

        System.out.println("\n --- Task 06 --- \n");
        System.out.println(reverseStringWords("Java is fun"));
    }


    public static int firstDuplicateNumber(int[] arr){
        ArrayList<Integer> container = new ArrayList<>();

        for (int i : arr) {
            if(container.contains(i))return i;
            else container.add(i);
        }

        return -1;
    }


    public static ArrayList<String> duplicateStrings(String[] arr){

        ArrayList<String>duplicate = new ArrayList<>();
        ArrayList<String>solution = new ArrayList<>();

        for (int i = 0; i < arr.length - 1 ; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if(arr[i].equalsIgnoreCase(arr[j]) && !duplicate.contains(arr[i].toLowerCase())){

                    duplicate.add(arr[i].toLowerCase());
                    solution.add(arr[i]);

                }
            }
        }
        return solution;
    }


    public static String reversedArray(String[] arr){

        ArrayList<String> reversedArr = new ArrayList<>();

        for (int i = arr.length-1 ; i >= 0 ; i--) {

           reversedArr.add(arr[i]);
        }

        return Arrays.toString(reversedArr.toArray());



    }

    public static String reversedArray1(String[] arr){

        Collections.reverse(Arrays.asList(arr));

        return Arrays.toString(arr);


    }




    public static StringBuilder reverseStringWords(String sentence){

        String[] words = sentence.split(" ");

        StringBuilder reverseSentence = new StringBuilder();

        for (String word : words) {

            StringBuilder r = new StringBuilder(word);

            reverseSentence.append(r.reverse()).append(" ");

        }

        return reverseSentence;

    }

    public static String reverseStringWords2(String str){
        String reversedStr = " ";
        String[] strAsArr = str.trim().split("\\s+");

        for (String s : strAsArr) {
            reversedStr += new StringBuilder(s).reverse()+ " ";

        }
        return reversedStr.trim();
    }

}
