package Projects;

public class Project_01 {
    public static void main(String[] args) {

        System.out.println("\n====Task-01====\n");

        String name="Yousef";

        System.out.println("My name is = " + name);

        System.out.println("\n====Task-02====\n");

        char nameCharacter1 = 'Y', nameCharacter2 = 'o',nameCharacter3='u',nameCharacter4='s',nameCharacter5='e',nameCharacter6='f';

        System.out.println("Name letter 1 is = " + nameCharacter1);
        System.out.println("Name letter 2 is = " + nameCharacter2);
        System.out.println("Name letter 3 is = " + nameCharacter3);
        System.out.println("Name letter 4 is = " + nameCharacter4);
        System.out.println("Name letter 5 is = " + nameCharacter5);
        System.out.println("Name letter 6 is = " + nameCharacter6);

        System.out.println("\n====Task-03====\n");

        String myFavMovie = "No country for old men",myFavSong = "Shake me down", myFavCity = "DC",myFavActivity = "Playing pool", myFavSnack= "Yogurt";

        System.out.println("My favorite movie is = " + myFavMovie);
        System.out.println("My favorite Song is = " + myFavSong);
        System.out.println("My favorite City is = " + myFavCity);
        System.out.println("My favorite activity is = " + myFavActivity);
        System.out.println("My favorite Snack is = " + myFavSnack);

        System.out.println("\n====Task-04====\n");

        int myFavNumber = 100, numberOfStatesVisited = 15,numbersOfCountriesVisited = 5;

        System.out.println("My Favorite number is = " + myFavNumber);
        System.out.println("I have visited " + numberOfStatesVisited + " States");
        System.out.println("I have visited " + numbersOfCountriesVisited + " Countries");

        System.out.println("\n====Task-05====\n");

        boolean amIAtSchoolToday = false;

        System.out.println("I am at School today = " + amIAtSchoolToday);

        System.out.println("\n====Task-06====\n");

        boolean isWeatherNiceToday = false;

        System.out.println("The weather is nice today = " + isWeatherNiceToday);










    }
}
