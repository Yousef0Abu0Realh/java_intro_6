package Projects;

import java.util.ArrayList;
import java.util.Arrays;

public class Project_07 {
    public static void main(String[] args) {

        System.out.println("\n --- Task 01 --- \n");

        String[] words = {"foo","", " ", "foo bar", "java is fun", " ruby "};

        System.out.println(countMultipleWords(words));

        System.out.println("\n --- Task 02 --- \n");

        ArrayList<Integer> numbs = new ArrayList<>(Arrays.asList(2, -5, 6, 7, -10, -78, 0, 15));

        System.out.println(removeNegative(numbs));

        System.out.println("\n --- Task 03 --- \n");

        System.out.println(validatePassword("Abcd123!"));

        System.out.println("\n --- Task 04 --- \n");

        System.out.println(validateEmailAddress("abcd@gmail.com"));
        System.out.println(validateEmailAddress("ab@gmail.com"));



    }
    // task 01 method
    public static int countMultipleWords(String[] arr){
        int counter = 0;


        for (String s : arr) {

            if(s.trim().split(" ").length > 1) counter++;
        }
        return counter;
    }
    // task 02 method
    public static ArrayList<Integer> removeNegative(ArrayList<Integer> numbs){

        numbs.removeIf(x -> x < 0);

        return numbs;
    }

    public static boolean validatePassword(String str){
        boolean hasDigit = false, hasUpper = false, hasLower = false, hasSpecial = false;

        for (int i = 0; i < str.length(); i++) {
            if(Character.isUpperCase(str.charAt(i))) hasUpper = true;
            else if(Character.isDigit(str.charAt(i))) hasDigit = true;
            else if(Character.isLowerCase(str.charAt(i))) hasLower = true;
            else hasSpecial = true;

        }
        return (str.length() > 7 && str.length() < 17 && !str.contains(" ") && hasUpper && hasDigit && hasLower && hasSpecial);
    }
    public static boolean validateEmailAddress(String str){

        boolean hasEmailName = false, notMoreAt = false, hasAddressName = false, hasCharsAfterDot = false, hasDot = false;
        int countAt = 0, countDot = 0;
        if(1 < str.substring(0, str.indexOf('@')).length()) hasEmailName = true;
        for (int i = 0; i < str.length(); i++) {
            if(str.charAt(i) == '@') countAt++;
            else if(str.charAt(i) == '.') countDot++;
        }
        if(countAt == 1) notMoreAt = true;
        if(1 < str.substring(str.indexOf('@') + 1, str.indexOf('.')).length()) hasAddressName = true;
        if(countDot == 1) hasDot = true;
        if(1 < str.substring(str.indexOf('.') + 1).length()) hasCharsAfterDot = true;
        return (hasEmailName && notMoreAt && hasAddressName && hasCharsAfterDot && hasDot);
    }







}
