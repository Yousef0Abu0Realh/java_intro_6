package Projects;

import java.util.Random;
import utilities.ScannerHelper;

public class Project_05 {
    public static void main(String[] args) {


        System.out.println("\n ---- Task 01 ---- \n");

        System.out.println("Hey user write something down");

        String str = ScannerHelper.getString();

        int spaces = 1;

        for (int i = 0; i < str.length(); i++) {

            if(Character.isSpaceChar(str.charAt(i))) spaces++;

        }
        System.out.println((spaces > 1)?("This sentence has " + spaces + " words" ):("This sentence does not have multiple words"));


        System.out.println("\n ---- Task 02 ---- \n");


        Random r = new Random();

        int r1 = r.nextInt(26);
        int r2 = r.nextInt(26);


        System.out.println("Int randomNumber1 = " + r1);
        System.out.println("Int randomNumber2 = " + r2);

        for (int i = Math.min(r1,r2); i <= Math.max(r1,r2) ; i++) {

            if (i % 5 != 0) System.out.println(i);

        }

        System.out.println("\n ---- Task 03 ---- \n");


        System.out.println("Hey user write something down");

        String str1 = ScannerHelper.getString();

        if(str1.length() >= 1){

            int countA = 0;
            for (int i = 0; i < str1.length(); i++) {

                str1 = str1.toLowerCase();

                if((str1.charAt(i)) == ('a'))countA++;

            }
            System.out.println("This Sentence has " + countA + " a or A letters.");

        }else System.out.println("This sentence does not have any characters.");

        System.out.println("\n ---- Task 04 ---- \n");

        System.out.println("Hey user write something down");

        String str2 = ScannerHelper.getString();

        if (str2.length()-1 >= 0){

            String reverse = "";
            for (int i = str2.length()-1; i >= 0 ; i--) {

                reverse += str2.charAt(i);

            }
            System.out.println(reverse);
            System.out.println((reverse.equals(str2))?("This word is a palindrome"):("This is not a palindrome"));

        } else System.out.println("this word does not have 1 or more characters");



        System.out.println("\n ---- Task 05 ---- \n");



        for (int i = 1; i <= 9 ; i++) {



            for (int j = 1; j <= 9 - i; j++) {

                System.out.print("   ");
            }


            for (int k = 1; k <= i * 2 - 1 ; k++) {
                System.out.print(" * ");
            }
            System.out.println();

        }




    }
}
