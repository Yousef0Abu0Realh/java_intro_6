package Projects;

import java.util.Arrays;
import java.util.Objects;

public class Project_06 {
    public static void main(String[] args) {

        System.out.println("\n --- task 01 --- \n");
        int[] numbers = {10, 7, 7, 10, -3, 10, -3};
        findGreatestAndSmallestWithSort(numbers);
        System.out.println("\n --- task 02 --- \n");
        findGreatestAndSmallestWithoutSort(numbers);
        System.out.println("\n --- task 03 --- \n");
        int[] numbers2 = {10,5,6,7,8,5,15,15};
        findSecondGreatestAndSmallestWithSort(numbers2);
        System.out.println("\n --- task 04 --- \n");
        findSecondGreatestAndSmallestWithoutSort(numbers2);
        System.out.println("\n --- task 05 --- \n");
        String[] words = {"foo", "bar", "Foo", "bar","6","abc","6","xyz"};
        findDuplicatedElementsInAnArray(words);
        System.out.println("\n --- task 06 --- \n");
        String[] words2 ={"pen","eraser","pencil","pen","123","abc","pen"};
        findMostRepeatedElementInAnArray(words2);

    }

    // Task 01 Method

    public static void findGreatestAndSmallestWithSort(int[] nums){

        if(nums.length > 0){
        Arrays.sort(nums);
        System.out.println("Smallest = " + nums[0]);
        System.out.println("Greatest  = " + nums [nums.length -1]);}
        else System.out.println("The Array contain no data");
    }

    // Task 02 Method

    public static void findGreatestAndSmallestWithoutSort(int[] nums){

        if(nums.length > 0){
            int greatest = nums[0] ;
            int smallest = nums[0] ;
            for (int num : nums) {

                if(greatest < num) greatest = num;
                else if (smallest > num) smallest = num;
            }
            System.out.println("Smallest = " + smallest);
            System.out.println("Greatest = " + greatest);
        }else System.out.println("The Array contain no data");
    }

    // Task 03 Method

    public static void  findSecondGreatestAndSmallestWithSort(int[] nums){

        if(nums.length > 0){
            Arrays.sort(nums);
            int minimum = nums[0];
            int maximum = nums[nums.length-1];

            int secondMinimum = 0;
            int secondMaximum = 0;

            for (int i = 1; i < nums.length - 1 ; i++) {

                if (nums[i] != minimum){ secondMinimum = nums[i];
                break;}
            }
            for (int i = nums.length-1; i > 0; i--) {
                if (nums[i] != maximum){ secondMaximum = nums[i];
                    break;}
            }

            System.out.println("Second Smallest = " + secondMinimum);
            System.out.println("Second Greatest = " + secondMaximum);


        }
        else System.out.println("There are no numbers in the Array");
    }

    // Task 04 Method

    public static void findSecondGreatestAndSmallestWithoutSort(int[] nums){

        if( nums.length > 0 ){
           int minimum = 100;
           int maximum = 0;
           int secondMinimum = 100;
           int secondMaximum = 0;

            for (int num : nums) {

                if(num > secondMaximum && num < maximum)secondMaximum = num;
                if(num > maximum){
                    secondMaximum = maximum;
                    maximum = num;
                }
                if (num < secondMinimum && num > minimum)secondMinimum = num;
                if (num < minimum){
                    secondMinimum = minimum;
                    minimum = num;
                }

            }

            System.out.println(secondMaximum);
            System.out.println(secondMinimum);


        }
        else System.out.println("There are no numbers in the Array");

    }

    // Task 05 Method

    public static void  findDuplicatedElementsInAnArray(String[] arr){
        System.out.println(Arrays.toString(arr));


        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1 ; j < arr.length ; j++) {

                if(Objects.equals(arr[i], arr[j])) System.out.println(arr[j]);

            }
            
        }

    }

    public static void findMostRepeatedElementInAnArray(String[] arr){
        int maxCount = 0;
        String mostRepeated = "";


        for (int i = 0; i < arr.length; i++) {
            int count = 1;
            for (int j = arr.length - 1; j >= 0  ; j--) {

                //String[] words2 ={"pen","eraser","pencil","pen","123","abc","pen"}

                if (arr[i].equals(arr[j])) {
                    count++;
                }
            }

            if (count > maxCount) {
                maxCount = count;
                mostRepeated = arr[i];
            }
        }


        System.out.println(mostRepeated);
    }
}

