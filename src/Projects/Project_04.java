package Projects;

import utilities.ScannerHelper;

public class Project_04 {
    public static void main(String[] args) {

        System.out.println("\n Task 01 \n");

        System.out.println("Hey user give me a string with more than eight characters.");

        String str1 = ScannerHelper.getString();

        if (str1.length() >= 8) {

            String sub1 = str1.substring(0, 4);
            String sub2 = str1.substring(str1.length() - 4, str1.length());
            String sub3 = str1.substring(4, str1.length() - 4);

            System.out.println(sub2 + sub3 + sub1);

        } else System.out.println("This string is less than 8 Characters");

        System.out.println("\n Task 02 \n");

        System.out.println("Hey user can you give me a sentence");

        String sentence = ScannerHelper.getString();

        if (sentence.contains(" ")) {

            String firstWord = sentence.substring(0, sentence.indexOf(" "));
            String middle = sentence.substring(sentence.indexOf(" "), sentence.lastIndexOf(" "));
            String lastWord = sentence.substring(sentence.lastIndexOf(" "), sentence.length());

            System.out.println(lastWord + " " + middle + " " + firstWord);
        } else {
            System.out.println("This sentence does not have 2 or more words to swap");
        }

        System.out.println("\n Task 03 \n");

        String str2 = "These books are so stupid";
        String str3 = "I like idiot behavoirs";
        String str4 = "I had some stupid t-shirt in the past and also some idiot look shoes";
        System.out.println(str2.replace("stupid", "nice"));
        System.out.println(str3.replace("idiot", "nice"));
        System.out.println(str4.replace("stupid", "nice").replace("idiot", "nice"));

        System.out.println("\n Task 04 \n");

        String name = ScannerHelper.getFirstName();

        if (name.length() > 2) {
            if (name.length() == 3) System.out.println(name.charAt(2));
            else System.out.println((name.substring(2, name.length() - 2)));
        } else {
            System.out.println(("Invalid input"));
        }

        System.out.println("\n Task 05 \n");

        System.out.println("Hey user can you enter your favorite country");

        String favCountry = ScannerHelper.getString();

        System.out.println((favCountry.length() > 4) ? (favCountry.substring(2, favCountry.length() - 2)) : ("Invalid input"));


        System.out.println("\n Task 06 \n");

        String address = ScannerHelper.getAddress();

        System.out.println(address.replace('A', '*').replace('a', '*').replace('E', '#').replace('e', '#').replace('I', '+').replace('i', '+').replace('U', '$').replace('u', '$').replace('O', '@').replace('o', '@'));


    }
}
