package Projects;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.util.Random;

public class Project_03 {
    public static void main(String[] args) {
        System.out.println("\n Task - 01 \n");

        String s1 = "24";
        String s2 = "5";

        int i1 = Integer.parseInt(s1);
        int i2 = Integer.parseInt(s2);

        System.out.println("The sum of " + s1 + " and " + s2 + " = " + (i1 + i2));
        System.out.println("The subtraction of " + s1 + " and " + s2 + " = " + (i1 - i2));
        System.out.println("The division of " + s1 + " and " + s2 + " = " + ((double) i1 / (double) i2));
        System.out.println("The multiplication of " + s1 + " and " + s2 + " = " + (i1 * i2));
        System.out.println("The remainder of " + s1 + " and " + s2 + " = " + (i1 % i2));

        System.out.println("\n Task - 02 \n");

        int r0 = (int)((Math.random()*35) + 1);

        System.out.println((r0 == 2 || r0 == 3 || r0 == 5 || r0 == 7 || r0 == 11 || r0 == 13 || r0 == 17 || r0 == 19 || r0 == 23 || r0 == 29 || r0 == 31)?("Random number = " + r0):(r0 + " IS NOT A PRIME NUMBER"));

        System.out.println("\n Task - 03 \n");

        Random r1 = new Random();

        int rand1 = r1.nextInt(50) + 1;
        int rand2 = r1.nextInt(50) + 1;
        int rand3 = r1.nextInt(50) + 1;

        if(rand1 > rand2 && rand1 > rand3){
            if (rand2>rand3){
                System.out.println("Lowest number is = " + rand3);
                System.out.println("Middle number is = " + rand2);
                System.out.println("Highest number is = " + rand1);
            }
            else{
                System.out.println("Lowest number is = " + rand2);
                System.out.println("Middle number is = " + rand3);
                System.out.println("Highest number is = " + rand1);
            }
        } else if (rand2 > rand1 && rand2 > rand3) {

            if (rand1 >rand3){
                System.out.println("Lowest number is = " + rand3);
                System.out.println("Middle number is = " + rand1);
                System.out.println("Highest number is = " + rand2);
            }
            else{
                System.out.println("Lowest number is = " + rand1);
                System.out.println("Middle number is = " + rand3);
                System.out.println("Highest number is = " + rand2);
            }
        }else {
            if(rand1 > rand2){
                System.out.println("Lowest number is = " + rand2);
                System.out.println("Middle number is = " + rand1);
                System.out.println("Highest number is = " + rand3);
            }else {
                System.out.println("Lowest number is = " + rand1);
                System.out.println("Middle number is = " + rand2);
                System.out.println("Highest number is = " + rand3);
            }
        }

        System.out.println("\n Task - 04 \n");

        char c ='A';

        if(c >= 97 && c<= 122) System.out.println("The letter is lowercase");
        else if (c >= 65 && c <= 90) System.out.println("The letter is uppercase");
        else System.out.println("Invalid character detected");


        System.out.println("\n Task - 05 \n");
        char e = 'A';

        if(e == 65 || e == 73 || e == 69 || e == 79 || e == 85 || e == 97 || e == 101 ||e == 105 ||e == 111 || e == 117) System.out.println("The letter is a vowel");
        else if (e == 'B' || e == 'C' || e == 'D' || e == 'F' || e == 'G' || e == 'H' || e == 'J' || e == 'K' || e == 'L' || e == 'M' || e == 'N' || e == 'P' || e == 'Q' || e == 'R' || e == 'S' || e == 'T' || e == 'V' || e == 'W' || e == 'X' || e == 'Y' || e == 'Z' || e == 'b' || e == 'c' || e == 'd' || e == 'f' || e == 'g' || e == 'h' || e == 'j' || e == 'k' || e == 'l' || e == 'm' || e == 'n' || e == 'p' || e == 'q' || e == 'r' || e == 's' || e == 't' || e == 'v' || e == 'w' || e == 'x' || e == 'y' || e == 'z') System.out.println("The letter is a consonant");
        else System.out.println("Invalid character detected");

        System.out.println("\n Task - 06 \n");

        char e2 = '*';

        System.out.println(((e2 >=32 && e2 <= 47) || (e2 >= 58 && e2 <= 64) || (e2 >= 91 && e2 <= 96) || (e2 >= 123 && e2 <= 126))?("Special character is = " + e2):("Invalid character detected"));

        System.out.println("\n Task - 07 \n");
        
        char q = '@';

        if((q >= 97 && q<= 122) || (q >= 65 && q <= 90) ) System.out.println("The character is letter");
        else if (q >= 48 && q <= 57) System.out.println("The character is digit");
        else System.out.println("The character is a special character");


    }
    }
