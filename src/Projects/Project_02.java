package Projects;

import java.time.Year;
import java.util.Scanner;

import static java.time.Year.now;

public class Project_02 {
    public static void main(String[] args) {

        System.out.println("\n --- Task 01 --- \n");

        Scanner input = new Scanner(System.in);

        System.out.println("Please enter 3 numbers");

        int num1 = input.nextInt();
        int num2 = input.nextInt();
        int num3 = input.nextInt();

        System.out.println("The product of the numbers entered is = " + (num1*num2*num3));

        System.out.println("\n --- Task 02 --- \n");

        System.out.println("What is your first name?");

        String firstName = input.next();

        System.out.println("What is your last name?");

        String lastName = input.next();

        System.out.println("What is your year of birth?");

        int year = input.nextInt();

        int thisYear = Year.now().getValue();

        System.out.println(firstName + " " + lastName + " age is  = " + (thisYear-year));

        System.out.println("\n --- Task 03 --- \n");

        System.out.println("What is your full name?");

        input.nextLine();

        String fullName = input.nextLine();

        System.out.println("What is your weight in kilograms");

        double weight = input.nextDouble();

        System.out.println(fullName + " weight is = " + Math.round((weight)*(2.205)));

        System.out.println("\n --- Task 04 --- \n");

        System.out.println("What is your full name?");

        input.nextLine();

        String fullName1 = input.nextLine();

        System.out.println("What is your age?");

        int age1 = input.nextInt();

         System.out.println("What is your full name?");

        input.nextLine();

        String fullName2 = input.nextLine();

        System.out.println("What is your age?");

        int age2 = input.nextInt();

         System.out.println("What is your full name?");

        input.nextLine();

        String fullName3 = input.nextLine();

        System.out.println("What is your age?");

        int age3 = input.nextInt();

        System.out.println(fullName1 + " age is " + age1);
        System.out.println(fullName2 + " age is " + age2);
        System.out.println(fullName3 + " age is " + age3);
        System.out.println("The average age is " + ((age1+age2+age3)/3));
        System.out.println("The eldest age is " + Math.max(Math.max(age1,age2),age3));
        System.out.println("The youngest age is " + Math.min(Math.min(age1,age2),age3));
















    }
}
