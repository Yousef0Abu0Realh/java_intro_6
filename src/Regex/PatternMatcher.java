package Regex;

import utilities.ScannerHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternMatcher {
    public static void main(String[] args) {
        String input = ScannerHelper.getString();
        System.out.println(Pattern.matches("[a-z0-9_-]{3,11}", input));


        Pattern pattern = Pattern.compile("Java");
        Matcher matcher = pattern.matcher("I love Java, Java is fun");

        System.out.println(pattern);

        System.out.println(pattern.toString());
        System.out.println(pattern.pattern());

        System.out.println(matcher.matches());

        int counter = 0;
        while(matcher.find()){
            counter++;
            System.out.println(matcher.group());
            System.out.println(matcher.start());
            System.out.println(matcher.end());
        }
        System.out.println( counter);
    }
}
