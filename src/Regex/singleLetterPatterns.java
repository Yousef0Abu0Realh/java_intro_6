package Regex;

import java.util.regex.Pattern;

public class singleLetterPatterns {
    public static void main(String[] args) {
        System.out.println(Pattern.matches("[xyz]","xyz"));
        System.out.println(Pattern.matches("[a-z&&[^pqrs]]","n"));
        System.out.println(Pattern.matches("[a-z&&[^pqrs]]","r"));
        System.out.println(Pattern.matches("[^xyz]","y"));

        String regex = "[a-zA-Z0-9_-]{9,15}";

        System.out.println(Pattern.matches(regex,"BalsiLwaDi01"));

    }
}
