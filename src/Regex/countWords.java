package Regex;

import utilities.ScannerHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class countWords {
    public static void main(String[] args) {

        System.out.println("Hey user give me a sentence");

        String sentence = ScannerHelper.getString();

        if (sentence.length() > 0){
            String[] words = sentence.split(" ");
            for (String word : words) {
                System.out.println(word);
            }
            System.out.println("This sentence contains " + words.length + " words");

        }
        else System.out.println("This sentence contains 0 words");


        //regex way
        String regex = "[A-Za-z]{1,}";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(sentence);
        int wordCount = 0;

        while(matcher.find()){
            System.out.println(matcher.group());
            wordCount++;
        }
        System.out.println("this sentence contains " + wordCount +  " words");

    }
}
