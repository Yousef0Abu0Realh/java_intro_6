package Regex;

import utilities.ScannerHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidUsername {
    public static void main(String[] args) {

        System.out.println("Hey user give me a username");

        String input = ScannerHelper.getString();


        // Way 01
        if (Pattern.matches("[a-zA-Z0-9]{5,10}",input)){
            System.out.println("Valid username");
        }
        else System.out.println("Error! Username must be 5 to 10 characters long and can only contain letters and numbers");


        // Way 02

        if(input.matches("[a-zA-Z0-9]{5,10}")) System.out.println("Valid username");
        else  System.out.println("Error! Username must be 5 to 10 characters long and can only contain letters and numbers");

        // Way 03

        Pattern pattern = Pattern.compile("[a-zA-Z0-9]{5,10}");
        Matcher matcher = pattern.matcher(input);

        if (matcher.matches()) System.out.println("Valid username");
        else System.out.println("Error! Username must be 5 to 10 characters long and can only contain letters and numbers");
    }
}
