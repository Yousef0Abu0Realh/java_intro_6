package Regex;

import java.util.regex.Pattern;

public class RemoveAll {
    public static void main(String[] args) {
        String str = "Apple";
        int vowelCounter = 0;

        for (char c : str.toCharArray()){
            if(Character.toLowerCase(c) == 'a' || Character.toLowerCase(c) == 'e' || Character.toLowerCase(c) == 'i'  || Character.toLowerCase(c) == 'o' ||  Character.toLowerCase(c) == 'u'){
                vowelCounter++;
            }
        }
        System.out.println("The word has " + vowelCounter + " vowels");

        str = str.replaceAll("[^aieouAEIOU]","");

        System.out.println(str);

        String phoneNumberRegex = "[(]?[0-9]{3}[)]?-[0-9]{3}-[0-9]{4}";

        System.out.println(Pattern.matches(phoneNumberRegex,"(123)-222-2222"));


    }
}
