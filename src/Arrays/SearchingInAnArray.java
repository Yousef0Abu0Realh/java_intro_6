package Arrays;

import java.util.Arrays;

public class SearchingInAnArray {
    public static void main(String[] args) {

        int[] numbers = {3, 10 , 8, 5, 5};

        int count7 = 0;

       for (int number : numbers) {

            if (number == 7) count7++;

        }

       if (count7 == 0) System.out.println(false);
       else System.out.println(true);




       boolean has7 = false;

        for (int number : numbers) {

            if (number == 7){
                has7 = true;
                break;
            }
        }
        System.out.println(has7);


        System.out.println("\n ---- Binary search way ---- \n");

        Arrays.sort(numbers);

        System.out.println(Arrays.binarySearch(numbers,5));
        System.out.println(Arrays.binarySearch(numbers,10));
        System.out.println(Arrays.binarySearch(numbers,3));
        System.out.println(Arrays.binarySearch(numbers,7));
        System.out.println(Arrays.binarySearch(numbers,15));
        System.out.println(Arrays.binarySearch(numbers,1));

    }
}
