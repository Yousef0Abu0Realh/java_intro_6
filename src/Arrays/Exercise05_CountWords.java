package Arrays;

import utilities.ScannerHelper;

public class Exercise05_CountWords {
    public static void main(String[] args) {
        System.out.println("Hey user, write something down");

        String str1 = ScannerHelper.getString();

        String[] strToArray = str1.split(" ");

        int actualWords = 0;

        for (String s : strToArray) {

            if(!s.isEmpty()) actualWords++;

        }

        System.out.println(actualWords);

    }
}
