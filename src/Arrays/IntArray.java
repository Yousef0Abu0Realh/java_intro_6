package Arrays;

import java.util.Arrays;

public class IntArray {
    public static void main(String[] args) {


        int[] numbers = new int[6];

        numbers[0] = 5;
        numbers[2] = 15;
        numbers[4] = 25;

        System.out.println(Arrays.toString(numbers));

        numbers [0] = 7;

        for (int i = 0; i < numbers.length; i++) {

            System.out.println(numbers[i]);

        }

       for(int number:numbers){
           System.out.println(number);
        }
    }
}
