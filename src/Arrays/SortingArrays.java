package Arrays;

import java.util.Arrays;

public class SortingArrays {

    public static void main(String[] args) {

        int[] numbers = {5,3,10};

        String[] names = {"Alex","ali","John","James"};



       Arrays.sort(names);
       Arrays.sort(numbers);

        System.out.println(Arrays.toString(names));
        System.out.println(Arrays.toString(numbers));

    }
}
