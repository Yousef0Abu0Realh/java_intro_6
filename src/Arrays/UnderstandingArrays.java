package Arrays;

import java.util.Arrays;

public class UnderstandingArrays {
    public static void main(String[] args) {

        String[] cities = {"Chicago" , "Miami" , "Toronto", "Rome" , "Berlin"};

        System.out.println(cities.length); // gives the length of the array

        System.out.println(cities[0]); // prints Chicago

        System.out.println(cities[1]); // prints Miami

        System.out.println(cities[2]); // prints Toronto


        System.out.println(Arrays.toString(cities)); // prints the whole array



        // printing with a loop

        System.out.println("\n --- for i loop --- \n");

        for (int i = 0; i < cities.length; i++) {

            System.out.println(cities[i]);
        }


        System.out.println("\n --- for each loop --- \n");

        for(String city : cities) {

            System.out.println(city);
        }
    }
}
