package Arrays;

public class Exercise01_CountNumbers{
    public static void main(String[] args) {

        int[] numbers = {-1, 3, 0, 5, -7, 10, 8, 0, 10, 0};

        int counter = 0;

        for (int number : numbers) {

            if (number < 0) counter++;

        }

        System.out.println(counter);


        System.out.println("\n --- count evens --- \n");


        int counter2 = 0;


        for (int number : numbers) {

            if (number % 2 == 0) counter2++;

        }

        System.out.println(counter2);


        System.out.println("\n --- sum of nums --- \n");

        int sum = 0;

        for (int i = 0; i < numbers.length; i++) {

            sum += numbers[i];
        }

        System.out.println(sum);

    }
}
