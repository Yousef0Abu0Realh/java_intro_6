package Arrays;

import java.util.Arrays;

public class Exercise02_CountStrings {
    public static void main(String[] args) {

        String[] countries = new String[3];

        countries[1] = "Spain";

        System.out.println(countries[1]);
        System.out.println(countries[2]);

        countries[0] = "Belgium";
        countries[2] = "Italy";

        System.out.println(Arrays.toString(countries));

        Arrays.sort(countries);

        System.out.println(Arrays.toString(countries));

        for (int i = 0; i < countries.length; i++) {

            System.out.println(countries[i]);

        }

        for (String country : countries) {

            System.out.println(country);

        }


        int counter = 0;


        for (String country : countries) {

            if (country.length() == 5)counter++;

        }

        System.out.println(counter);






        int containsI = 0;


        for (String country : countries) {

            if (country.toLowerCase().contains("i"))containsI++;

        }

        System.out.println(containsI);


    }
}
