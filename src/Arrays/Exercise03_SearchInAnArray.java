package Arrays;

import com.sun.xml.internal.ws.addressing.WsaActionUtil;

import java.util.Arrays;

public class Exercise03_SearchInAnArray {
    public static void main(String[] args) {
        String[] objects = {"Remote", "Mouse", "Mouse", "Keyboard", "Ipad"};

        boolean containsM = false;

        for (String object : objects) {


            if (object.equals("Mouse")){
                containsM = true;
                break;
            }
        }
        System.out.println(containsM);


        Arrays.sort(objects);

        System.out.println(Arrays.binarySearch(objects, "Mouse") >= 0);
    }
}
