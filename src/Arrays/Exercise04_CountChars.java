package Arrays;

import utilities.ScannerHelper;

public class Exercise04_CountChars {
    public static void main(String[] args) {

        System.out.println("Hey user, write something down");

        String str = ScannerHelper.getString();

        char[] strToChar = str.toCharArray();

        int count = 0;

        for (char c : strToChar) {

            if (Character.isLetter(c)) count++;
        }

        System.out.println(count);

    }
}
