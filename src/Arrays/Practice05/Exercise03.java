package Arrays.Practice05;

import java.util.Arrays;
import java.util.Random;

public class Exercise03 {
    public static void main(String[] args) {

        Exercise03.minANDMaxWithoutUsingSort();
        Exercise03.minANDMaxUsingSort();


    }
    public static void minANDMaxUsingSort(){
        Random r = new Random();
        int[] numbers = new int[5];
        numbers[0] = r.nextInt(10);
        numbers[1] = r.nextInt(10);
        numbers[2] = r.nextInt(10);
        numbers[3] = r.nextInt(10);
        numbers[4] = r.nextInt(10);


        Arrays.sort(numbers);

        System.out.println(Arrays.toString(numbers));

        System.out.println(numbers[0]);
        System.out.println(numbers[4]);

    }
    public static void minANDMaxWithoutUsingSort(){

        Random r = new Random();

        int[] numbers = new int[5];
        numbers[0] = r.nextInt(10);
        numbers[1] = r.nextInt(10);
        numbers[2] = r.nextInt(10);
        numbers[3] = r.nextInt(10);
        numbers[4] = r.nextInt(10);


        int min = numbers[0];
        int max = numbers[0];

        for (int number : numbers) {

            if (number > max) max =number;
            else if (number < min) min =number;

        }

        System.out.println(Arrays.toString(numbers));

        System.out.println(min);
        System.out.println(max);


    }

}
