package Arrays.Practice05;

import java.util.Scanner;

public class Exercise02 {
    public static void main(String[] args) {
        String[] colors = {"Red","Blue","Yellow","White"};

        getShortestAndLongest(colors);
    }

    static Scanner input = new Scanner(System.in);

    public static void getShortestAndLongest(String[]words){

        String shortest = words[0];
        String longest = words[0];

        for (String word : words) {

            if(word.length() > longest.length()) longest = word;
            else if (word.length()< shortest.length()) shortest = word;
        }
        System.out.println("The longest word is " + longest);
        System.out.println("The shortest word is " + shortest);
        }

    }

