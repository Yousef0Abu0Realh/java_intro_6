package Arrays;

import java.util.Arrays;

public class TwoDimensionalArrays {
    public static void main(String[] args) {

        String[][] students = {
                {"Meerim", "Alina", "Carmela", "Ayat"},
                {"Yahya", "Adam","Louie"},
                {"Dima", "Lesia", "Pinar"}
        };

        //How to print an inner array

        System.out.println(Arrays.toString(students[0]));
        System.out.println(Arrays.toString(students[1]));
        System.out.println(Arrays.toString(students[2]));

        //how to print an element

        System.out.println(students[0][1]);

        // how to print the length of two-dimensional array

        System.out.println(students.length);

        // how to print inner array length

        System.out.println(students[0].length);
        System.out.println(students[1].length);
        System.out.println(students[2].length);

        // how to loop 2-dimensional array

        for (String[] innerArray : students) {

            System.out.println(Arrays.toString(innerArray));
        }

        // how to loop 2-dimensional array for each element

        for (String[] inner : students) {

            // each inner array is here

            for (String s : inner) {

                // each element is here

                System.out.println(s);
            }
        }

        // printing with fori

        System.out.println("Printing the elements with a fori loop");

        for (int i = 0; i < students.length; i++) {

            for (int j = 0; j < students[i].length; j++) {
                System.out.println(students[i][j]);
            }
        }

        int[][] numbers = new int[5][3];

        System.out.println(Arrays.deepToString(numbers));

        numbers[3][1] = 9;

        System.out.println(Arrays.deepToString(numbers));

        numbers[2][0] = 5;

        System.out.println(Arrays.deepToString(numbers));

        numbers[0][0] = 7;

        System.out.println(Arrays.deepToString(numbers));







    }
}
