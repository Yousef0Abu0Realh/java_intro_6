package character_class;

import utilities.ScannerHelper;

public class CountCharacters {
    public static void main(String[] args) {


        System.out.println("Hey user give me a string");
        String str = ScannerHelper.getString();

        int digit = 0;
        int letter = 0;

        for (int i = 0; i < str.length(); i++) {

            if (Character.isLetter(str.charAt(i))) letter++;
            else if (Character. isDigit(str.charAt(i))) digit++;

        }

        System.out.println("This string has " + letter + " letters and " + digit + " digits");



        System.out.println("Hey user give me a string");
        String str2 = ScannerHelper.getString();

        int lower = 0;
        int upper = 0;

        for (int i = 0; i < str2.length(); i++) {

            if (Character.isUpperCase(str2.charAt(i))) upper++;
            else if (Character. isLowerCase(str2.charAt(i))) lower++;

        }

        System.out.println("This string has " + upper + " Uppercase letters and " + lower + " LowerCase letters");



        System.out.println("Hey user give me a string");
        String str3 = ScannerHelper.getString();

        int length = str3.length();

        for (int i = 0; i < str3.length(); i++) {

            if(Character.isLetterOrDigit(str3.charAt(i))) length--;
            else if(Character.isSpaceChar(str3.charAt(i))) length --;

        }

        System.out.println("The number of special characters is = " + length);



        System.out.println("Hey user give me a string");
        String str4 = ScannerHelper.getString();

        int letters = 0;
        int numbers = 0;
        int uppercaseLetters = 0;
        int lowercaseLetters = 0;
        int specialChars = 0;
        int spaces = 0;


        for (int i = 0; i < str4.length(); i++) {

            if(Character.isLetter(str4.charAt(i))) letters++;{
                if(Character.isUpperCase(str4.charAt(i))) uppercaseLetters++;
                else if(Character.isLowerCase(str4.charAt(i))) lowercaseLetters++;
            }
            if(Character.isDigit(str4.charAt(i))) numbers++;
            else if(Character.isSpaceChar(str4.charAt(i))) spaces++;
            else if(!Character.isLetterOrDigit(str4.charAt(i))) specialChars++;

        }

        System.out.println("The number of special characters is = " + specialChars);
        System.out.println("The number of letters is = " + letters);
        System.out.println("The number of digit characters is = " + numbers);
        System.out.println("The number of space characters is = " + spaces);
        System.out.println("The number of uppercase characters is = " + uppercaseLetters);
        System.out.println("The number of lowercase characters is = " + lowercaseLetters);



        
    }
}
