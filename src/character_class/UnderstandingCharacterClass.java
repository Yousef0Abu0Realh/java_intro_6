package character_class;

import utilities.ScannerHelper;

import static java.lang.Character.isUpperCase;

public class UnderstandingCharacterClass {
    public static void main(String[] args) {

        System.out.println("Hey user give me a string");


        String str = ScannerHelper.getString();

        System.out.println(Character.isUpperCase(str.charAt(0)));
        System.out.println(Character.isLowerCase(str.charAt(0)));
        System.out.println(Character.isLetter(str.charAt(0)));
        System.out.println(Character.isLetterOrDigit(str.charAt(0)));
        System.out.println(Character.isDigit(str.charAt(0)));
        System.out.println(Character.isWhitespace(str.charAt(0)));
        System.out.println(Character.isSpaceChar(str.charAt(0)));

    }
}
