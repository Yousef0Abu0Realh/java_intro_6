package escape_sequences;

public class Exercise01 {
    public static void main(String[] args) {
        System.out.println("\tToday, I woke up at 7:00 AM.\nThen, I went to \n\n\n\n\n\t gym and did my daily exercise.\nLater, after having breakfast, I will need to study.");

        System.out.print("\tJava is a high-level, class-based,\nobject-oriented programming language that is\ndesigned to have as few implementation dependencies\nas possible.\n\n\t Java was originally developed by\nJames Gosling at Sun Microsystems. It was released\nin May 1995 as a core component of Sun Microsystems'\nJava platform.\n\n\t As of March 2022, Java 18 is the latest version, while\nJava 17, 11 and 8 are the current long-term support (LTS)\nversions.");

        /*  Java is a high-level, class-based,
object-oriented programming language that is
designed to have as few implementation dependencies
as possible.

    Java was originally developed by
James Gosling at Sun Microsystems. It was released
in May 1995 as a core component of Sun Microsystems'
Java platform.

    As of March 2022, Java 18 is the latest version, while
Java 17, 11 and 8 are the current long-term support (LTS)
versions.
*/

    }
}
