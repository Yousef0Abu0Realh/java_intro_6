package Operators.increment_decrement_operators;

public class PostIncrementPostDecrement {
    public static void main(String[] args) {
        int num = 10;

        System.out.println(num++); // increase after the statement is run

        System.out.println(num);

        System.out.println(++num); // increase for the statement

        System.out.println("\n task02 \n");

        int n1 = 5 , n2 = 7;

        n1++;

        n1 += n2 ;

        System.out.println(n1);

        System.out.println("\n task 3 \n");

        int i1 = 10;

        --i1; //decrease it by 1 right now - 9

        i1--; // decrease it by one for the next use

        System.out.println(--i1);//7

        System.out.println("\n task 4 \n");

        int number1 = 50;

        number1 -= 25;

        number1 -= 10;

        System.out.println(number1--);






    }
}
