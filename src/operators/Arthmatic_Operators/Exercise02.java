package Operators.arithmatic_operators;

public class Exercise02 {
    public static void main(String[] args) {

        double salary = 90000;

        System.out.println("Monthly = $" + salary / 12);
        System.out.println("Weekly = $" + salary / 52 );
        System.out.println("Bi-Weekly = $" + salary / 26);
    }
}
