package Operators.shorthand_operators;

public class ShorthandPractice {
    public static void main(String[] args) {
        int age = 45;

        System.out.println("Age in 2023 = " + age);

        age += 5;

        System.out.println(age);

        age -= 20;
    }
}
