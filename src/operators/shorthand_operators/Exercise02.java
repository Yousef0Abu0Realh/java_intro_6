package Operators.shorthand_operators;

import java.util.Scanner;

public class Exercise02 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Hey user, please tell me your balance");

        double balance = input.nextDouble();

        System.out.println("The initial balance = $" + balance);

        System.out.println("Now tell me how much you spent on your First transaction");

        double transaction1 = input.nextDouble();

        balance -= transaction1;

        System.out.println("the balance after the first transaction is = $" + balance);

        System.out.println("Now tell me how much you spent on your Second transaction");

        double transaction2 = input.nextDouble();

        balance -=transaction2;

        System.out.println("the balance after the second transaction is = $" + balance);

        System.out.println("Now tell me how much you spent on your Third transaction");

        double transaction3 = input.nextDouble();

        balance -= transaction3;

        System.out.println("the current balance is = $" + balance);

    }
}
