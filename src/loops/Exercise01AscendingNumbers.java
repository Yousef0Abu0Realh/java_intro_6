package loops;

public class Exercise01AscendingNumbers {
    public static void main(String[] args) {
        int num = 1;

        for (int i = 0; i < 10; i++) {
            System.out.println(num++);
        }

    }
}
