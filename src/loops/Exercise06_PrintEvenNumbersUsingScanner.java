package loops;

import utilities.ScannerHelper;

import java.util.Scanner;

public class Exercise06_PrintEvenNumbersUsingScanner {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Hey user give me 2 positive numbers");

        int num1 = input.nextInt();
        int num2 = input.nextInt();

        if (num1 >num2){
            for (int i = num2; i < num1 ; i++) {
                if(i % 2 == 0) System.out.println(i);

            }
        }
        else{
            for (int i = num1; i < num2 ; i++) {
                if(i % 2 == 0) System.out.println(i);

            }
        }

        System.out.println("\n------second way-------\n");

        for (int i = Math.min(num1,num2); i <= Math.max(num1,num2) ; i++) {

            if(i % 2 == 0) System.out.println(i);

        }
    }
}
