package loops;

import utilities.ScannerHelper;

public class Exercise08_CountCharacterInAString {
    public static void main(String[] args) {


        System.out.println("Hey user give me a string");

        String str = ScannerHelper.getString();

        int counter = 0;

        for (int i = 0; i < str.length(); i++) {

            if(str.charAt(i) == 'A' || str.charAt(i) == 'a')counter++;

        }
        System.out.println(counter);
    }
}
