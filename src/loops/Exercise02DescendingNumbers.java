package loops;

public class Exercise02DescendingNumbers {

    public static void main(String[] args) {

        for (int i = 100; i > -1;  i--) {

            System.out.println(i);

        }
    }
}
