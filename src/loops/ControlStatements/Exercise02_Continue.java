package loops.ControlStatements;

public class Exercise02_Continue {

    public static void main(String[] args) {

        for (int i = 0; i <= 100; i++) {

           if(i % 13 == 0) continue;
           else System.out.println(i);

        }
    }
}
