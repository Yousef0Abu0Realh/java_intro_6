package loops.ControlStatements;

import java.util.Scanner;

public class Exercise01_Break {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int n1 = input.nextInt();
        int n2 = input.nextInt();


        for (int i = Math.max(n1,n2); i >= Math.min(n1,n2) ; i--) {

            if (i <10) break;
            else System.out.println(i);

        }
        System.out.println("End of program");



    }
}
