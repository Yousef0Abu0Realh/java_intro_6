package loops;

public class Exercise11_FizzBuzz {

    public static void main(String[] args) {


        int start = 1;

        while(start <= 30){
            if(start % 3 == 0 && start % 5 == 0) System.out.println("FizzBuzz");
            else if(start % 3 == 0) System.out.println("Fizz");
            else if(start % 5 == 0) System.out.println("Buzz");
            else System.out.println(start);
            start++;
        }
    }
}
