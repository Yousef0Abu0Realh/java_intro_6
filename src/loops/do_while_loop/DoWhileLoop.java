package loops.do_while_loop;

public class DoWhileLoop {
    public static void main(String[] args) {

        int num = 0;

        do{
            System.out.println(num);
            num++;
        }
        while(num <= 10);
    }
}
