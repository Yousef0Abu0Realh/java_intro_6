package loops;

public class Exercise04_PrintNumberBy {

    public static void main(String[] args) {


        for (int i = 0; i <= 10; i++) {
            System.out.println(i*5);
        }

        for (int i = 50; i > 1 ; i--) {

            if(i % 5 == 0) System.out.println(i);

        }
    }
}
