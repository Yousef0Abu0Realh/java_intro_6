package loops;

import java.util.Scanner;

public class Exercise10_SumOfNumbersByUser {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int counter = 1;
        int sum =0;

        System.out.println("Hey user give me a number");

        while(counter <= 5){

            if (counter > 1) System.out.println("Now give me another number");

            int num = input.nextInt();
            sum += num;

            counter ++;
        }

        System.out.println("The sum of the numbers is =" + sum);


    }
}
