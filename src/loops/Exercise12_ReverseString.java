package loops;

import utilities.ScannerHelper;

public class Exercise12_ReverseString {
    public static void main(String[] args) {

        String firstname = ScannerHelper.getFirstName();

        String reverse = "";

        for (int i = firstname.length()-1; i >= 0; i--) {

            reverse += (firstname.charAt(i));

        }
        System.out.println(reverse);

    }
}
