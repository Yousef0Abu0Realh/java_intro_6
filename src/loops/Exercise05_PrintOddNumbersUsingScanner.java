package loops;

import java.util.Scanner;

public class Exercise05_PrintOddNumbersUsingScanner {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Hey user give me a number");

        int num = input.nextInt();

        for (int i = 0; i <= num ; i++) {

           if (i % 2 == 1) System.out.println(i);

        }
    }
}
