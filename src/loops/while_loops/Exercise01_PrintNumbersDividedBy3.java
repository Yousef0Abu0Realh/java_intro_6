package loops.while_loops;

public class Exercise01_PrintNumbersDividedBy3 {
    public static void main(String[] args) {

        //stable method

        int num = 1;
        while(num<=100){
            if(num % 3 == 0) System.out.println(num);

            num++;
        }

        // hard coded way

        int k = 3;
        while(k <= 100){
            System.out.println(k);
            k += 3;
        }
    }
}
