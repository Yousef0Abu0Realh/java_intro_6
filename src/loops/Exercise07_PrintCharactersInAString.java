package loops;

import utilities.ScannerHelper;

public class Exercise07_PrintCharactersInAString {
    public static void main(String[] args) {

        System.out.println("Hey user write something down");

        String str = ScannerHelper.getString();

        for (int i = 0; i < str.length(); i++) {

            System.out.println(str.charAt(i));

        }

        System.out.println("");
    }
}
