package loops.Practices;

import utilities.ScannerHelper;

public class Exercise06 {

    public static void main(String[] args) {


        int num;

        int sum = 0;

        int counter = 0;


        do {

            if (counter == 1) System.out.println("The number given is not more than 100");
            else if (counter > 1) System.out.println("The sum of the given numbers is not more than 100");
            num = ScannerHelper.giveNumber();

            sum += num;

            counter ++;
        }
        while(sum < 100);
         if (counter == 1) System.out.println("The number given is greater than 100");
         else if (counter > 1) System.out.println("The sum of the given numbers is greater than 100");



    }
}
