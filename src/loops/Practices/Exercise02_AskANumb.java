package loops.Practices;

import utilities.ScannerHelper;

import java.util.Scanner;

public class Exercise02_AskANumb {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int num;

        do {

            System.out.println("Give me a Number");


            num = input.nextInt();

            if(num < 10) System.out.println("This number is less than 10");

        }
        while( num < 10);

        System.out.println("This number is more than or equal to ten");






        System.out.println("\n ---- While loop method ---- \n");





        int num2 = ScannerHelper.giveNumber();

        while (num2 < 10){

            num2 = ScannerHelper.giveNumber();

            if(num2 < 10) System.out.println("This number is less than 10");
        }
        System.out.println("This number is more than or equal to ten");


    }
}
