package loops.Practices;

public class Exercise01_FooBar {
    public static void main(String[] args) {


        for (int i = 1; i <= 10; i++) {

            if (i % 5 == 0 && i % 2 == 0) System.out.println("FooBar");
            else if (i % 2 == 0) System.out.println("Foo");
            else if (i % 5 == 0) System.out.println("bar");
            else System.out.println(i);

        }
    }
}
