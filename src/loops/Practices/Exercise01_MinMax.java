package loops.Practices;

import utilities.ScannerHelper;

public class Exercise01_MinMax {


    public static void main(String[] args) {


        int num = ScannerHelper.giveNumber();
        int num2 = ScannerHelper.giveNumber();


        for (int i = Math.min(num,num2); i <= Math.max(num,num2) ; i++) {

            if (i != 5) System.out.println(i);


        }
    }
}
