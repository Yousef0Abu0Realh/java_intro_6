package loops.Practices;

import utilities.ScannerHelper;

public class Exercise04 {
    public static void main(String[] args) {

        System.out.println("Hey user enter a String");
        
        String str = ScannerHelper.getString();
        
        int counter = 0;

        for (int i = 0; i < str.length(); i++) {

            str = str.toLowerCase();

            if(str.charAt(i) == 'a' || str.charAt(i) == 'e' || str.charAt(i) == 'i' || str.charAt(i) == 'o' || str.charAt(i) == 'u') counter++;

        }

        System.out.println("There are " + counter + " vowel letters in this String");



    }
}
