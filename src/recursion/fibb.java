package recursion;

import java.util.ArrayList;
import java.util.Vector;

public class fibb {
    public static void main(String[] args) {
        System.out.println(FibbArray(7));

    }

    public static ArrayList<Integer> FibbArray(int IndexOfNum) {
        ArrayList<Integer> FibbNums = new ArrayList<Integer>();
        if (IndexOfNum == 0) {
            FibbNums.add(0);
        } else if (IndexOfNum == 1) {
            FibbNums.add(1);
            FibbNums.add(0);
        } else {
            FibbNums = FibbArray(IndexOfNum - 1);
            int newValue = FibbNums.get(IndexOfNum - 1) + FibbNums.get(IndexOfNum - 2);
            FibbNums.add(newValue);
        }

        return FibbNums;
    }

}
