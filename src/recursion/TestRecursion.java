package recursion;

public class TestRecursion {

    public static void main(String[] args) {

        System.out.println(sum1ToNIterative(5));
        System.out.println(sum1ToNIterative(4));
        System.out.println(sum1ToNIterative(10));


        System.out.println(sum1toNRecursive(5));
        System.out.println(factorialRecursive(3));

    }


    public static int sum1ToNIterative(int n){
        int sum = 0;

        for (int i = 1; i <= n ; i++) {

            sum += i;

        }
        return sum;
    }


    public static int sum1toNRecursive(int n){
        if(n != 0 ) return  n + sum1toNRecursive(n-1);
        return 0;
    }

    public static int factorialRecursive(int n){
        if(n != 1 ) return  n * factorialRecursive(n-1);
        return 1;
    }
}
