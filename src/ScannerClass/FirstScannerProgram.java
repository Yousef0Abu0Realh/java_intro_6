package ScannerClass;

import java.util.Scanner;

public class FirstScannerProgram {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Give me a number");

        int num01 = input.nextInt();

        System.out.println("The number given is " + num01);

        System.out.println("Please enter your first name.");

        String name = input.next();

        System.out.println("the users name is " + name);

        System.out.println("Please enter your full name");

        String fullName = input.next();

        System.out.println("Your full name is " + fullName);








    }
}
