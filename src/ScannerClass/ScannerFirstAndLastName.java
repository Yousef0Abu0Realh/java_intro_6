package ScannerClass;

import java.util.Scanner;

public class ScannerFirstAndLastName {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Hey user can you tell me your first name");

        String firstName = input.nextLine();

        System.out.println("Users first name is " + firstName);

        System.out.println("Now tell me your last name");

        String lastName = input.nextLine();

        System.out.println("Users last name is " + lastName);

        String fullName = firstName + " "+ lastName;

        System.out.println("Your full  name is " + fullName);

    }

}
