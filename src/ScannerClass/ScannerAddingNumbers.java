package ScannerClass;

import java.util.Scanner;

public class ScannerAddingNumbers {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter number 1");

        int num1 = input.nextInt();

        System.out.println("Number 1 = " + num1);


        System.out.println("Please enter number 2");

        int num2 = input.nextInt();

        System.out.println("Number 2 = " + num2);


        System.out.println("Please enter number 3");

        int num3 = input.nextInt();

        System.out.println("Number 3 = " + num3);

        int sum = num1+num2+num3;

        System.out.println("The sum of the numbers you entered is  " + sum);
    }
}
