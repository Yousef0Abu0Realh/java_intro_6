import java.util.ArrayList;
import java.util.Arrays;

public class clas {

        public static int[] multiply2(int[] arr){

            int[] by2 = new int[arr.length];

            for (int i = 0; i < arr.length; i++) {

                by2[i] = arr[i]*2;
            }

            return by2;

        }


    public static  int findMax(ArrayList<Integer>arr){

        int max = Integer.MIN_VALUE;

        for (Integer o : arr) {

            if(max < o) max = o;

        }
        return  max;
    }

    public static void main(String[] args) {

            ArrayList<Integer> numbs = new ArrayList<>(Arrays.asList(-20, 35, 70, 4, 5));

           System.out.println(Arrays.toString(multiply2(new int[]{3, 5, 0, 1})));
           System.out.println(findMax(numbs));
    }
}
