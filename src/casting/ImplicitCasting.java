package casting;

public class ImplicitCasting {
    public static void main(String[] args) {

        // implicit casing //

        short num1 = 45;

        int num2 = num1;

        float decimal1 = 34.5F;

        double decimal2 = decimal1;


    }
}
