package casting;

public class StringToPrimitive {
    public static void main(String[] args) {
        int num1 = 5, num2 = 10;

        System.out.println(num1+num2 + 5);//20 - numeric 15
        System.out.println("" + num1 + num2);//510
        System.out.println("" + (num1 + num2) + 5);//15 -text 15


        //converting numbers to a string //

        System.out.println("" + num1 + num2); // "510"
        System.out.println(String.valueOf(num1) + String.valueOf(num2)); //"510"

        System.out.println(String.valueOf(num2+num1) + String.valueOf(7 + num2)); // 1517


        //converting String to primitives

        String price = "1597.06";

        System.out.println(Double.parseDouble(price) - 10.0); // 1587.06
    }
}
