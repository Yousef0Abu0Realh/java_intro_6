package casting;

public class ExplicitCasting {
    public static void main(String[] args) {

        // Explicit casting //

        long num1 = 232324423;

        byte num2 = (byte) num1;

        System.out.println(num2);
    }
}
