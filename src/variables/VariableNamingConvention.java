package variables;

public class VariableNamingConvention {
    public static void main(String[] args) {
        boolean isFemale= true;
        System.out.println(isFemale);

        double d1 = 5.5;

        double $d;

        String class$_ = "B6";

        double money= 5.0;
        double mOney= 5.0;
        double Money= 5.0;

        double d2 = 2.5;
        System.out.println(d2);

        d2 = 3.4;

        System.out.println(d2);


    }
}
