package variables;

public class CreatingMultipleVariables {
    public static void main(String[] args) {
        int age1;
        int age2;
        int age3;

        int age4, age5, age6;
        age4 = 15;
        age6 = 20;

        System.out.println(age4);
        System.out.println(age6);


        double d1;
        double d2= 50.5;
        double d3;
    }
}
