package Practices;

import utilities.ScannerHelper;

public class Exercise01_StringMethods {
    public static void main(String[] args) {

        System.out.println("\n **** task 01 **** \n");

        System.out.println("Hey user can you write something down");

        String str = ScannerHelper.getString();

        System.out.println("The given string is = " + str);

        System.out.println("\n **** task 02 **** \n");

        //first way

        System.out.println((str.length() > 0 )?("The length of the given string is = " + str.length()):("The string is empty"));

        // second way

        if(str.equals("") ) System.out.println("The string is empty");
        else System.out.println("The length of the given string is = " + str.length());

        //third way

        if(str.isEmpty()) System.out.println("The string given is empty");
        else System.out.println("the length of the give string is = " + str.length());

        System.out.println("\n **** task 03 **** \n");


        System.out.println((str.length() >=1)?("the first letter of the given string is = " + str.charAt(0)):("the given string is empty"));

        System.out.println("\n **** task 04 **** \n");


        System.out.println((str.length() >=1)?("the last letter of the given string is = " + str.charAt(str.length()-1)):("the given string is empty"));

        System.out.println((str.length() > 0)?("the last letter of the given string is = " + str.charAt(str.length()-1)):("the given string is empty"));

        System.out.println((str.length() == 0)?("the given string is empty"):("the last letter of the given string is = " + str.charAt(str.length()-1)));


        System.out.println("\n **** task 05 **** \n");

        if (str.toUpperCase().contains("A") || str.toUpperCase().contains("E") || str.toUpperCase().contains("I") || str.toUpperCase().contains("O") || str.toUpperCase().contains("U")) System.out.println("This string has a vowel");
        else System.out.println("This string does not have a vowel");


        System.out.println("\n **** task 06 *** \n");

        if (str.length() < 3) System.out.println("Length of the string is less than 3");
        else if (str.length() >= 3 && str.length() % 2 == 0) System.out.println( "" + str.charAt(str.length()/2 - 1)  + str.charAt(str.length()/2));
        else if (str.length() >= 3 && str.length() % 2 == 1) System.out.println(str.charAt(str.length()/2));




    }
}
