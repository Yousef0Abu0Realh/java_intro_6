package Practices;

import utilities.ScannerHelper;

public class Exercise03_StringMethods {
    public static void main(String[] args) {

        System.out.println("Hey user write something down");

        String str = ScannerHelper.getString();

        if (str.length() >= 4) {
            System.out.println("first 2 characters of the string are = " + (str.charAt(0)) + str.charAt(1));
            System.out.println("last 2 characters of the string are = " + (str.charAt(str.length()-2)) + str.charAt(str.length() - 1));
            System.out.println("the other characters are =" + (str.substring(2,str.length()-2)));
        }
        else System.out.println("Invalid the length of the string is to short");


    }
}
