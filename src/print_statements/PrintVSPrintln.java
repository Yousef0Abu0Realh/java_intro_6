package print_statements;

public class PrintVSPrintln {
    public static void main(String[] args) {


       System.out.println("\n **yo** \n");
       System.out.println("Today is Sunday the 29th");

       // print statement

        System.out.print("hello world");
        System.out.print("Today is sunday");

    }
}
