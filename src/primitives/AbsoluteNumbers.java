package primitives;

public class AbsoluteNumbers {

    public static void main(String[] args) {


        System.out.println("\n----byte----\n");
        byte myNum = 45;
        System.out.println(myNum);

        System.out.println("The max value of byte = " + Byte.MAX_VALUE);
        System.out.println("The min value of byte = " + Byte.MIN_VALUE);

        System.out.println("\n----short----\n");

        short numShort = 150;
        System.out.println(numShort);

        System.out.println("The max value of short = " + Short.MAX_VALUE);
        System.out.println("The min value of short = " + Short.MIN_VALUE);

        System.out.println("\n----int----\n");
        int myInt = 2000000000;
        System.out.println(myInt);

        System.out.println("The max value of int is = " + Integer.MAX_VALUE);
        System.out.println("The min value of int is = " + Integer.MIN_VALUE);

        System.out.println("\n----Long----");

        long myLong = 100000000000000000L;
        System.out.println(myLong);

    }

}
