package data_time;

import javax.crypto.spec.PSource;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Locale;

public class _01_LocalDateTime {
    public static void main(String[] args) {

        System.out.println("\n ----- Local Date ----- \n");
        LocalDate currentDate = LocalDate.now();

        System.out.println(currentDate);

        System.out.println(currentDate.format(DateTimeFormatter.ofPattern("MM/dd/yyyy")));

        System.out.println(currentDate.getYear());


        System.out.println("\n ----- Local Time ----- \n");
        LocalTime currentTime = LocalTime.now();

        System.out.println(currentTime);
        System.out.println(currentTime.format(DateTimeFormatter.ofPattern("hh:mm:ss a")));
        System.out.println(currentTime.plusHours(2).format(DateTimeFormatter.ofPattern("hh:mm:ss a")));

        System.out.println("\n ----- LocalDateTime ----- \n");

        LocalDateTime currentDateTime = LocalDateTime.now();


        System.out.println(currentDateTime);
        System.out.println(currentDateTime.format(DateTimeFormatter.ofPattern("MMMM dd, yyyy h:mm ac")));


        System.out.println(Period.between(LocalDate.of(2023, Month.JANUARY, 23), LocalDate.now()).get(ChronoUnit.MONTHS));


    }
}
