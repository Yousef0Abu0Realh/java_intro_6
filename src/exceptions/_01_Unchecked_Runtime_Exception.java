package exceptions;

import utilities.ScannerHelper;

public class _01_Unchecked_Runtime_Exception {

    public static void main(String[] args) {

       String name = ScannerHelper.getFirstName();

       try {
           System.out.println(name.charAt(5));

           String s = null;
           System.out.println(s.toUpperCase());

           Thread.sleep(1000);
       }catch (Exception e){

           System.out.println(e.getMessage());

       }

       finally {
           System.out.println("I am here to run all the time");
       }
        System.out.println("End of program");
    }
}
