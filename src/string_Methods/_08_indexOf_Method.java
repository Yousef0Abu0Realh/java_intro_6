package string_Methods;

public class _08_indexOf_Method {
    public static void main(String[] args) {
        String str = "TechGlobal";

        System.out.println(str.indexOf('h'));

        System.out.println(str.indexOf('y'));

        System.out.println(str.indexOf("Tech"));

        System.out.println(str.indexOf("Global"));
    }
}
