package string_Methods;

public class _05_ToLowerCase_Method {
    public static void main(String[] args) {
        String str1 = "JAVA IS FUN";

        System.out.println(str1);
        System.out.println(str1.toLowerCase());

        char c = 'A';

        System.out.println(String.valueOf(c).toLowerCase());
    }
}
