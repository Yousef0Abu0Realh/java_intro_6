package string_Methods;

import utilities.ScannerHelper;

public class _13_Contains_Method {
    public static void main(String[] args) {

        String name = "John Doe";

        boolean containsJohn = name.contains("John");

        System.out.println(containsJohn);


        String str = ScannerHelper.getString();

        System.out.println(str.contains(" ") && str.endsWith("."));



    }
}
