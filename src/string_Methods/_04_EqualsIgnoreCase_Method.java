package string_Methods;

public class _04_EqualsIgnoreCase_Method {
    public static void main(String[] args) {
        /*

         */

        String str1 = "Hello";
        String str2 = "Hi";
        String str3 = "hello";
        String str4 = "HeLloHI";

        System.out.println((str1.equalsIgnoreCase(str2))); // false
        System.out.println((str1.equalsIgnoreCase(str3))); // true
        System.out.println((str1.concat(str2)).equalsIgnoreCase(str4));//true


    }
}
