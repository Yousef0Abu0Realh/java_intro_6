package string_Methods;

public class _14_Replace_Method {
    public static void main(String[] args) {


        String str = "TechGlobal";

        String str2 = "ABC123";

        System.out.println(str);
        System.out.println(str.replace("Tech",""));

        System.out.println(str2);
        System.out.println(str2.replace("ABC","abc"));

        System.out.println(str2.replace("2",""));
    }
}
