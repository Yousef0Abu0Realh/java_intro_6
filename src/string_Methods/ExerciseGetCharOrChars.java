package string_Methods;

public class ExerciseGetCharOrChars {
    public static void main(String[] args) {

        String name1 = "bilal";   // length 5 | Middle char is index of 2
        String name2 = "Matthew";  // length 7 | Middle char is index of 3
        String name3 = "Ronaldo";  // length 9 | Middle char is index of 4

        System.out.println(name1.charAt((name1.length()-1)/2));
        System.out.println(name2.charAt((name2.length()-1)/2));
        System.out.println(name3.charAt((name3.length()-1)/2));

        String name4 = "okan"; // length 4 | Middle char is index of 1 And 2
        String name5 = "Yousef"; // length 4 | Middle char is index of 1 And 2

        System.out.println("" + name4.charAt((name4.length()/2)-1) + (name4.charAt(name4.length()/2)));
        System.out.println("" + name5.charAt((name5.length()/2)-1) + (name5.charAt((name5.length()/2))));
    }
}
