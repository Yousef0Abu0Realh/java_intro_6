package string_Methods;

public class MethodChaining {
    public static void main(String[] args) {
        String str = "TechGlobal";

        System.out.println(str.toLowerCase());

        System.out.println(str.toLowerCase().contains("tech"));


        System.out.println(str.toUpperCase().substring(4).length());

        String sentence = "Hello, my name is John Doe. I am 30 years old and I go to school at TechGlobal";


        System.out.println(sentence.toLowerCase().replace("a","X").replace("e","X").replace("i","X").replace("o","X").replace("u","X"));



    }
}
