package string_Methods;

public class _09_Length_Method {
    public static void main(String[] args) {


        String str1 = "TechGlobal";
        String str2 = "Yousef";
        String str3 = "I am learning java and it is fun";

        System.out.println(str1.length());
        System.out.println(str2.length());
        System.out.println(str3.length());
        System.out.println("".length());
    }
}
