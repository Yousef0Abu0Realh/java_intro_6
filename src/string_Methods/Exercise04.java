package string_Methods;

public class Exercise04 {
    public static void main(String[] args) {

        String str = "I go to TechGlobal";

        String firstWord = str.substring(0,1);
        String secondWord = str.substring(2,5);
        String thirdWord = str.substring(5,8);
        String fourthWord = str.substring(8,18);

        System.out.println(firstWord);
        System.out.println(secondWord);
        System.out.println(thirdWord);
        System.out.println(fourthWord);
    }
}
