package string_Methods;

import java.util.Scanner;

public class ExerciseCharAt {
    public static void main(String[] args) {


        String str = "TechGlobal";            // last index is 9  | Length is 10
        String str2 = "Hello World";          // last index is 10 | length is 11
        String str3 = "I really love java";   // last index is 17 | length is 18

        System.out.println(str.charAt(4));
        System.out.println(str.charAt(9));

        System.out.println(" \n ***More dynamic way*** \n ");

        System.out.println(str.charAt(str.length()-1));
        System.out.println(str2.charAt(str2.length()-1));
        System.out.println(str3.charAt(str3.length()-1));

        Scanner input = new Scanner(System.in);

        System.out.println("Hey user say something");

        String str4 = input.nextLine();

        System.out.println(str4.charAt(str4.length()-1));
    }
}
