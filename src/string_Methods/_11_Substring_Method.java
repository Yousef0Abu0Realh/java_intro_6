package string_Methods;

public class _11_Substring_Method {
    public static void main(String[] args) {

        String str = "I love java a lot";

        String firstWord = str.substring(0,1);
        String secondWord = str.substring(2,6);
        String thirdWord = str.substring(7,11);

        System.out.println(firstWord);
        System.out.println(secondWord);
        System.out.println(thirdWord);

    }
}
