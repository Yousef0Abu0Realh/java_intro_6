package string_Methods;

import java.util.Scanner;

public class Exercise05 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Hey user write something");

        String str = input.nextLine().toLowerCase();

        System.out.println((str.startsWith("a")) && (str.endsWith("e")));
    }

}
