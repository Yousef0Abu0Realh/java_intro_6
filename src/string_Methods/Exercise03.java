package string_Methods;

public class Exercise03 {
    public static void main(String[] args) {

        String str = "The best is Java";

        System.out.println(str.substring(12,16));


        str = str.substring(str.indexOf("Java"));

        System.out.println(str);



        //Third way
        str = str.substring(str.lastIndexOf(' ')).trim();
        System.out.println(str);

        

    }
}
