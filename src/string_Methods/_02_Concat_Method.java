package string_Methods;

public class _02_Concat_Method {
    public static void main(String[] args) {

        /*
        1.return type
        2.returning string
        3.non-static
        4.in takes a String as an argument

         */

        String str1 = "Tech";

        String str2 = "Global";

        System.out.println(str1.concat(str2).toUpperCase());
        System.out.println(str1.concat(str2));

    }
}
